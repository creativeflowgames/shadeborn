class_name Clue extends Resource

# Warning: 
#   If entries are removed or added before the last entry
#   make sure to double check Enemy's `clue_priorities`
enum ClueCategory { CHEST_NOISE, PLAYER_FOOTSTEP, ARTIFACT_THROWN, 
	DOOR_OPEN, TARGET_LOST, TREASURE_DETECTED, LOUD_NOISE }


var category: int
var position: Vector2
var instigator: Node2D = null
var search_duration: float = 0.0


func _init(_category: int, _position: Vector2, _instigator: Node2D = null, _search_duration: float = 0.0) -> void:
	category = _category
	position = _position
	instigator = _instigator
	search_duration = _search_duration


func _to_string() -> String:
	return "Clue: %s @ (%.2f, %.2f) %s%s" % [
		ClueCategory.keys()[category], 
		position.x, 
		position.y, 
		instigator.to_string() if instigator else "",
		" for %.1fs" % search_duration if search_duration > 0 else ""]
