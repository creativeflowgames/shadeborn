tool
class_name StatusEffect
extends Resource

export(String) var name : String = ""
export(float, 0, 999) var duration : float = 0
export var modifier_list = {}


func apply_modifier(attribute: String, base_value: float) -> float:
	if not modifier_list.has(attribute):
		return base_value

	return modifier_list[attribute].apply(base_value)


func _to_string() -> String:
	var s : String = "%s (%0.1f sec)" % [name, duration]
	for key in modifier_list.keys():
		s += "\n\t%s  %s" % [key, modifier_list[key]]
	return s
