tool
class_name Modifier
extends Resource

export(float) var amount = 1.0


func apply(base_value: float) -> float:
	return base_value * amount


func _to_string() -> String:
	return "%.2f" % amount
