tool
extends CanvasLayer

export(PackedScene) var RecoverDialog
export(NodePath) var ShadeWidget
export(NodePath) var IlluminanceWidget
export(NodePath) var UsePromptWidget
export(NodePath) var SacrificeProgressWidget
export(NodePath) var ScoreWidget
export(NodePath) var InventoryWidget

var player : Player setget _set_player


onready var shade = get_node(ShadeWidget)
onready var illuminance = get_node(IlluminanceWidget)
onready var use_prompt = get_node(UsePromptWidget)
onready var sacrifice_progress = get_node(SacrificeProgressWidget)
onready var score = get_node(ScoreWidget)
onready var inventory = get_node(InventoryWidget)


func _get_configuration_warning() -> String:
	for thing in [RecoverDialog, ShadeWidget, IlluminanceWidget, 
		UsePromptWidget, SacrificeProgressWidget, ScoreWidget,
		InventoryWidget]:
		if not thing:
			return "One or more properties are unset."
	return ""


func _set_player(value: Player) -> void:
	player = value
	if player:
		shade.init(player)
		illuminance.init(player)
		use_prompt.init(player)
		sacrifice_progress.init(player)
		score.init(player)
		inventory.init(player)


func _enter_tree() -> void:
	add_to_group(Group.HUD)


func show_recover_dialog(is_dead: bool) -> void:
	var dialog = RecoverDialog.instance()
	dialog.player = player
	add_child(dialog)
