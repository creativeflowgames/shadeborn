extends PanelContainer

export(PackedScene) var ScoreRowScene

export(NodePath) var LevelNameValue
export(NodePath) var ScoreRowContainer
export(NodePath) var TotalScoreValue


func show_dialog(level_name: String, score: Player.Score) -> void:
	get_node(LevelNameValue).text = level_name
	
	var score_row_container = get_node(ScoreRowContainer)
	for item in score.list:
		var score_row = ScoreRowScene.instance()
		score_row.init(item.category, item.name, item.value)
		score_row_container.add_child(score_row)
	
	get_node(TotalScoreValue).text = str(int(score.calculate()))


func _on_BackButton_pressed() -> void:
	var game = Group.get_single_node(Group.GAME)
	if game:
		game.return_to_menu()
	else:
		get_tree().reload_current_scene()
