extends PanelContainer

export(NodePath) var recover_button
export(NodePath) var message_label
export(String, MULTILINE) var dead_message
export(String, MULTILINE) var incapacitated_message

var player : Player

onready var button_sound = $ButtonSound
onready var game = Group.get_single_node(Group.GAME)


func _ready() -> void:
	get_node(recover_button).disabled = not player.can_recover()
	get_node(message_label).text = incapacitated_message if player.can_recover() else dead_message

	
func _on_QuitButton_pressed() -> void:
	visible = false
	button_sound.play()
	yield(button_sound, "finished")
	
	if game:
		game.return_to_menu()
	else:
		get_tree().quit()


func _on_RestartButton_pressed() -> void:
	visible = false
	button_sound.play()
	yield(button_sound, "finished")
	
	if game:
		game.restart_level()
	else:
		get_tree().reload_current_scene()


func _on_RecoverButton_pressed() -> void:
	player.recover()
	
	visible = false
	button_sound.play()
	yield(button_sound, "finished")
	queue_free()
