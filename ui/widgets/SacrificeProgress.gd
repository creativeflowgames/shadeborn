extends Control

var sacrifice_timer : Timer

onready var progress_bar := $ProgressBar
onready var tween : Tween = $Tween


func _ready() -> void:
	set_process(false)


func init(player: Player) -> void:
	sacrifice_timer = player.sacrifice_timer
	visible = false
	
	player.connect("sacrifice_state_changed", self, "_on_sacrifice_state_changed")
	set_process(true)


func _process(delta: float) -> void:
	progress_bar.value = 1 - (sacrifice_timer.time_left / sacrifice_timer.wait_time)


func _on_sacrifice_state_changed(new_value: String) -> void:
	match new_value:
		"hold_start":
			set_process(true)
			tween.stop_all()
			rect_scale = Vector2.ONE
			modulate = Color.white
			visible = true
		"cancel":
			set_process(false)
			tween.interpolate_property(self, "modulate", Color.white, Color.transparent, .50, Tween.TRANS_LINEAR, Tween.EASE_IN)
			tween.start()
			
			yield(tween, "tween_all_completed")
			visible = false
		"hold_end":
			set_process(false)
			tween.interpolate_property(self, "rect_scale:x", 1, 0, .25, Tween.TRANS_QUART, Tween.EASE_OUT)
			tween.start()
			
			yield(tween, "tween_all_completed")
			visible = false
			
