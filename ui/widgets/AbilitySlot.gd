tool
extends VBoxContainer

export(Color) var border_color = Color("b315d7") setget set_border_color, get_border_color
var ability_icon setget set_ability_icon, get_ability_icon
var shade_cost setget set_shade_cost, get_shade_cost
export(float, 1, 500, 1.0) var icon_size = 60.0 setget set_icon_size, get_icon_size
export(StyleBoxFlat) var border_style

onready var cost_label: Label = $ShadeCost


func _ready() -> void:
	$Border.set("custom_styles/panel", border_style)


func set_border_color(value: Color) -> void:
	if border_style:
		border_style.border_color = value


func get_border_color() -> Color:
	return border_style.border_color


func set_ability_icon(value: Texture) -> void:
	if not value:
		clear()
	else:
		$Border/Margin/Icon.texture = value


func get_ability_icon() -> Texture:
	return $Border/Margin/Icon.texture


func set_shade_cost(value: String) -> void:
	$ShadeCost.text = value


func get_shade_cost() -> String:
	return $ShadeCost.text


func set_icon_size(value: float) -> void:
	$Border/Margin/Icon.rect_min_size = Vector2(value, value)
	visible = false
	visible = true


func get_icon_size() -> float:
	return $Border/Margin/Icon.rect_min_size.x


func clear() -> void:
	$ShadeCost.text = ""
	$Border/Margin/Icon.texture = null
