extends Control


onready var tween = $Tween
onready var score_text = $Value

var old_score : float = 0.0



func _ready() -> void:
	score_text.text = "0"


func init(player: Player) -> void:
	set_score(player.score.calculate())
	player.connect("score_changed", self, "animate_score")


func set_score(value: float):
	score_text.text = "%d" % value


func animate_score(new_value: float):
	if old_score == new_value:
		return
	
	tween.stop_all()
	tween.interpolate_method(self, "set_score", old_score, new_value, 
		0.25, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.start()
	
	old_score = new_value
