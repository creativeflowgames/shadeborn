extends Control


func init(player: Player) -> void:
	_on_shade_changed(player.shade, player.MAX_SHADE)
	player.connect("shade_changed", self, "_on_shade_changed")


func _ready() -> void:
	$Bar.min_value = 0


func _on_shade_changed(new_value: float, max_value: float) -> void:
	$Bar.max_value = max_value
	$Bar.value = new_value
	$Label.text = String(int(new_value))
