extends VBoxContainer

signal button_pressed

var level setget _set_level

func _set_level(value: PackedScene) -> void:
	level = value
	$Label.text = level.get_state().get_node_name(0)

func _on_Button_pressed() -> void:
	emit_signal("button_pressed")
