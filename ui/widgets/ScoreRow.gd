extends HBoxContainer

export(Color) var positive_color = Color.green
export(Color) var negative_color = Color.red

func init(category: String, name: String, value: float) -> void:
	$Category.text = category
	$Name.text = name
	$Value.text = "%+.0f" % value
	$Value.set("custom_colors/font_color", positive_color if value > 0 else negative_color)
