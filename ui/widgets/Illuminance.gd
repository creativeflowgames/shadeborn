extends Control

onready var tween: Tween = $Tween
onready var texture: TextureRect = $TextureRect

export(Color) var lit = Color.yellow
export(Color) var unlit = Color.darkgray
export(float, 0, 10, 0.01) var animation_duration = 0.6


func init(player: Player) -> void:
	var illuminance = player.get_attribute("illuminance")
	texture.modulate = unlit.linear_interpolate(lit, illuminance)
	
	player.connect("illuminance_changed", self, "_on_illuminance_changed")


func _on_illuminance_changed(new_value: float) -> void:
	var new_color: Color = unlit.linear_interpolate(lit, new_value)
	tween.stop_all()
	tween.interpolate_property(texture, "modulate", 
		texture.modulate, new_color,
		animation_duration,
		Tween.TRANS_CIRC, Tween.EASE_OUT)
	tween.start()
