extends MarginContainer

signal toggled(button_pressed)

var group setget set_group
var text setget set_text
var pressed setget set_pressed, get_pressed

onready var button := $Button


func _enter_tree() -> void:
	$Button.connect("toggled", self, "_on_button_toggled")

func _on_button_toggled(button_pressed: bool):
	emit_signal("toggled", button_pressed)


func update2(button_pressed: bool) -> void:
	if button_pressed:
		button.mouse_filter = Control.MOUSE_FILTER_PASS
		button.focus_mode = Control.FOCUS_NONE
	else:
		button.mouse_filter = Control.MOUSE_FILTER_STOP
		button.focus_mode = Control.FOCUS_ALL


func set_group(value: ButtonGroup) -> void:
	$Button.group = value


func set_text(value: String) -> void:
	$Button.text = value


func set_pressed(value: bool) -> void:
	$Button.pressed = value


func get_pressed() -> bool:
	return $Button.pressed
