extends HBoxContainer

onready var primary_slot = $PrimarySlot
onready var secondary_slot = $SecondarySlot


func init(player: Player) -> void:
	player.inventory.connect("changed", self, "_on_inventory_changed")
	update_items(player.inventory.items)

	
func _on_inventory_changed(new_items: Array) -> void:
	update_items(new_items)


func update_items(items: Array) -> void:
	update_item(primary_slot, items[0] if len(items) > 0 else null)
	update_item(secondary_slot, items[1] if len(items) > 1 else null)


func update_item(slot: Control, item: Artifact) -> void:
	if item:
		match item.activation_type:
			Artifact.ActivationType.INSTANT:
				slot.shade_cost = "%d" % item.cost
			Artifact.ActivationType.CONTINUOUS:
				slot.shade_cost = "%d/s" % item.get_cost(1.0)
		slot.ability_icon = item.ability_icon
	else:
		slot.clear()
	
