extends Control

onready var label = $Label

func init(player: Player) -> void:
	player.connect("interactable_changed", self, "_on_interactable_changed")
	_on_interactable_changed()


func _on_interactable_changed(item: Node2D = null):
	if item:
		var action_key = PoolStringArray()
		for action in InputMap.get_action_list("use"):
			if action is InputEventKey:
				action_key.append(action.as_text())
			# if action is InputEventJoypadButton:
			# 	action_key.append(Input.get_joy_button_string(action.button_index))
		
		label.text = "%s %s (%s)" % [item.use_string_prefix, item.item_name, action_key.join(", ")]
		visible = true
	else:
		label.text = ""
		visible = false
