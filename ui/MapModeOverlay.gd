tool
extends TextureRect

export(String, "none", "light_map", "tile_id") var map_mode = "none" setget _set_map_mode


func _ready() -> void:
	if map_mode != "none":
		texture = create_image(funcref(self, map_mode))
	else:
		texture = null
	
	var map = Group.get_single_node(Group.MAP)
	var map_rect = map.get_used_rect()
	map_rect.position *= map.cell_size
	map_rect.size *= map.cell_size
	
	rect_global_position = map_rect.position
	rect_size = map_rect.size


func _set_map_mode(value: String) -> void:
	if not is_inside_tree():
		return
	if map_mode != value:
		map_mode = value
		if map_mode != "none":
			texture = create_image(funcref(self, map_mode))
		else:
			texture = null
		property_list_changed_notify()


func create_image(function: FuncRef) -> Texture:
	var map = Group.get_single_node(Group.MAP)
	var map_rect: Rect2 = map.get_used_rect()
	
	var image := Image.new()
	image.create(int(map_rect.size.x), int(map_rect.size.y), false, Image.FORMAT_RGBA8)
	
	image.lock()
	for y in map_rect.size.y:
		for x in map_rect.size.x:
			var pos = Vector2(x, y) + map_rect.position
			var color = function.call_func(pos)
			image.set_pixel(x, y, color)
	image.unlock()
	
	var _texture = ImageTexture.new()
	_texture.create_from_image(image, 0)
	return _texture


func tile_id(pos: Vector2) -> Color:
	var map = Group.get_single_node(Group.MAP)
	var num_tiles: int = map.tile_set.get_tiles_ids().size()
	var tile_id = map.get_cellv(pos)
	return Color.from_hsv(float(tile_id) / num_tiles, 0.7, 0.8, 0.9)


func light_map(pos: Vector2) -> Color:
	var map = Group.get_single_node(Group.MAP)
	var world_pos = map._map_to_world(pos)
	for light in get_tree().get_nodes_in_group(Group.STATIC_LIGHT):
		if light.global_position.distance_to(world_pos) < light.tile_radius * Config.tile_size + map.tile_size * 0.5:
			return ColorN("yellow", 0.5)
	return ColorN("black", 0.5)
