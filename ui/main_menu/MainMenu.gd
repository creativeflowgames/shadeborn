extends CanvasLayer


export(PackedScene) var LevelButtonScene
export(NodePath) var LevelButtonContainer

var game setget _set_game


func init() -> void:
	$TabContainer.current_tab = 0


func _set_game(value) -> void:
	game = value
	
	var level_list = get_node(LevelButtonContainer)
	
	while level_list.get_child_count() > 0:
		var child = level_list.get_child(0)
		level_list.remove_child(child)
		child.queue_free()
	
	for level in game.Levels:
		var level_button = LevelButtonScene.instance()
		level_button.level = level
		level_button.connect("button_pressed", game, "change_level", [level])
		level_list.add_child(level_button)

	
func _on_Quit_pressed() -> void:
	game.quit()


func _on_New_Game_pressed() -> void:
	$TabContainer.current_tab = 1


func _on_BackButton_pressed() -> void:
	$TabContainer.current_tab = 0


func _on_Controls_pressed() -> void:
	$TabContainer.current_tab = 2
