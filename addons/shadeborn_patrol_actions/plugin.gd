tool
extends EditorPlugin

const small_font = preload("res://ui/fonts/lato_regular_small.tres")

var enemy : Enemy
var patrol_actions : Patrol


func edit(object: Object) -> void:
	enemy = object
	patrol_actions = enemy.patrol_actions


func make_visible(visible: bool) -> void:
	# Called when the editor is requested to become visible.
	if not patrol_actions:
		return
	if not visible:
		patrol_actions = null
	update_overlays()
	

func handles(object: Object) -> bool:
	return object is Enemy and object.patrol_actions != null


func forward_canvas_draw_over_viewport(overlay: Control) -> void:
	if not patrol_actions:
		return
		
	if not patrol_actions.nonempty():
		return
	
	var path = PoolVector2Array()
	var labels = PoolStringArray()
	
	var global_transform = enemy.get_global_transform()
	var viewport_transform = enemy.get_viewport_transform()
	
	for action_index in range(patrol_actions.actions.size()):
		var action : Action = patrol_actions.actions[action_index]
		
		if action is ActionGoToObject:
			var target = enemy.get_node(action.target_node)
			if target:
				var point = viewport_transform * target.global_position
				path.append(point)
				labels.append(action_index + 1)
			else:
				print("%s (#%d) has no valid target" % [action.action_name, action_index])
		if action is ActionLookAt:
			var direction = action.direction
			path.append(Vector2.ZERO)
			labels.append("%d: %s" % [action_index + 1,Util.direction_string(direction)])
			
	
	if path and patrol_actions.behavior == Patrol.Behavior.LOOP:
		path.append(path[0])
		labels.append(labels[0])
	
	
	var draw_path = Array(path)
	while draw_path.has(Vector2.ZERO):
		draw_path.erase(Vector2.ZERO)
		
	overlay.draw_polyline(draw_path, Color.cadetblue, 3.0, true)
	
	
	var pos = viewport_transform * enemy.global_position
	for i in len(path):
		if path[i] == Vector2.ZERO:
			pos += Vector2.UP * 24
		else:
			pos = path[i]
		overlay.draw_string(small_font, pos, labels[i], Color.aqua)
	
