tool
extends EditorPlugin

var available_plugins: Array = [
	load("res://addons/shadeborn_tools/status_effect/StatusEffectInspectorPlugin.gd"),
	load("res://addons/shadeborn_tools/clue_priorities/CluePriorityListInspectorPlugin.gd")
]

var active_plugins = []


func _init() -> void:
	for plugin in available_plugins:
		active_plugins.append(plugin.new())


func _enter_tree() -> void:
	for plugin in active_plugins:
		add_inspector_plugin(plugin)


func _exit_tree() -> void:
	for plugin in active_plugins:
		remove_inspector_plugin(plugin)
