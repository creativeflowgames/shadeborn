tool
extends EditorProperty

var child_control = preload("StatusEffectEditor.tscn").instance()

func _init():
	add_child(child_control)
	set_bottom_editor(child_control)

	child_control.connect("value_changed", self, "_on_value_changed")


func _on_value_changed(value: Dictionary):
	emit_changed(get_edited_property(), value)

	
func update_property():
	var new_value = get_edited_object()[get_edited_property()]
	child_control.modifier_list = new_value
