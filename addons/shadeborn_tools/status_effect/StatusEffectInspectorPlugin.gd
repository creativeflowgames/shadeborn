extends EditorInspectorPlugin

var StatusEffectEditorProperty = preload("StatusEffectEditorProperty.gd")

func can_handle(object: Object) -> bool:
	return object is StatusEffect

func parse_property(object: Object, type: int, path: String, hint: int, hint_text: String, usage: int) -> bool:
	if type == TYPE_DICTIONARY:
		add_property_editor(path, StatusEffectEditorProperty.new())
		return true
	return false


