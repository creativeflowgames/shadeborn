tool
extends VBoxContainer


# Output (sends back model when values change)
signal value_changed(value)

const CluePriority = preload("CluePriorityEditor.tscn")

export(NodePath) var CLUE_PRIORITY_LIST
export(NodePath) var AVAILABLE_CLUE_LIST
export(NodePath) var LABEL_EMPTY

var clue_priorities setget _update_model

onready var available_clue_list : ItemList = get_node(AVAILABLE_CLUE_LIST)
onready var clue_priority_list : VBoxContainer = get_node(CLUE_PRIORITY_LIST)


func string_to_enum(category_string: String) -> int:
	return Clue.ClueCategory[category_string]


func sort_priority(a, b):
	if clue_priorities[string_to_enum(a)] > clue_priorities[string_to_enum(b)]:
		return true
	return false


# Input (updates model and trigger view)
func _update_model(value: Dictionary) -> void:
	clue_priorities = value
	_recreate_view_from_model()


# Resets and rebuild UI
func _recreate_view_from_model() -> void:
	reset_state()
	
	var available_categories : Dictionary = Clue.ClueCategory.duplicate()
	
	var active_categories = []
	
	# Fill available list and collect active
	for category_string in available_categories.keys():
		var category_id = available_categories[category_string]
		if category_id in clue_priorities:
			active_categories.append(category_string)
		else:
			var count = available_clue_list.get_item_count()
			available_clue_list.add_item(category_string.capitalize())
			available_clue_list.set_item_tooltip(count, category_string)
	
	# Sort by priority
	active_categories.sort_custom(self, "sort_priority")
	
	# Add to list in inspector
	for category_string in active_categories:
		set_clue_priority(category_string, clue_priorities[string_to_enum(category_string)])
	
	property_list_changed_notify()

	get_node(LABEL_EMPTY).visible = clue_priorities.empty()
	available_clue_list.get_parent().visible = available_clue_list.get_item_count() > 0


# Delete all `ModifierEditor`s
func reset_state() -> void:
	while clue_priority_list.get_child_count():
		var child = clue_priority_list.get_child(0)
		clue_priority_list.remove_child(child)
		child.queue_free()
	
	available_clue_list.clear()


func set_clue_priority(category_string: String, priority: int) -> void:
	var category_id = string_to_enum(category_string)
	clue_priorities[category_id] = priority
	
	# Add node to tree
	var new_clue_priority = CluePriority.instance()
	new_clue_priority.set_state(category_string, clue_priorities[category_id])
	
	new_clue_priority.connect("clue_priority_changed", self, "_on_clue_priority_changed")
	new_clue_priority.connect("clue_priority_removed", self, "_on_clue_priority_removed")

	clue_priority_list.add_child(new_clue_priority)


func _on_clue_priority_changed(category_string: String, new_value: int) -> void:
	clue_priorities[string_to_enum(category_string)] = new_value
	emit_signal("value_changed", clue_priorities)


func _on_clue_priority_removed(category_string: String) -> void:
	clue_priorities.erase(string_to_enum(category_string))
	emit_signal("value_changed", clue_priorities)


func _on_AvailableClueList_item_selected(index: int) -> void:
	var category_string : String = available_clue_list.get_item_tooltip(index)
	available_clue_list.remove_item(index)
	
	clue_priorities[string_to_enum(category_string)] = 0
	
	_recreate_view_from_model()
