extends EditorInspectorPlugin

const _PROPERTY_NAME = "clue_priorities"

var CluePriorityListEditorProperty = preload("CluePriorityListEditorProperty.gd")


func can_handle(object: Object) -> bool:
	# print(object.get_class())
	if object.get_class() == "SectionedInspectorFilter":
		return false
	return object.get(_PROPERTY_NAME) != null


func parse_property(object: Object, type: int, path: String, hint: int, hint_text: String, usage: int) -> bool:
	if type == TYPE_DICTIONARY and path == _PROPERTY_NAME:
		add_property_editor(path, CluePriorityListEditorProperty.new())
		return true
	return false


