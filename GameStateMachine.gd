extends StateMachine


export(PackedScene) var TutorialLevel: PackedScene
export(Array, PackedScene) var Levels: Array
export(PackedScene) var MainMenuScene: PackedScene

var level_scene: PackedScene
var level: Level
var main_menu: Node


func _ready() -> void:
	add_to_group(Group.GAME)
	
	add_state("start")
	add_state("main_menu")
	add_state("playing")
	add_state("quit")
	
	call_deferred("set_state", states.start)
	
	# Put tutorial as level 0
	Levels.push_front(TutorialLevel)
	
	level_scene = Levels.front()

	main_menu = MainMenuScene.instance()
	main_menu.game = self
	

func _enter_state(old_state, new_state):
	match new_state:
		states.start:
			call_deferred("set_state", states.main_menu)
		states.main_menu:
			# main_menu.init()
			add_child(main_menu)
		states.playing:
			level = level_scene.instance()
			level.game = self
			add_child(level)
			Debug.playing = true
		states.quit:
			get_tree().quit()


func _exit_state(old_state, new_state):
	match old_state:
		states.main_menu:
			remove_child(main_menu)
		states.playing:
			remove_child(level)
			level.queue_free()
			Debug.playing = false


func quit():
	set_state(states.quit)


func change_level(value: PackedScene = level_scene) -> void:
	level_scene = value
	call_deferred("set_state", states.playing)


func return_to_menu() -> void:
	call_deferred("set_state", states.main_menu)


func restart_level() -> void:
	change_level()
