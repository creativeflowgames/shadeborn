tool
extends Vision

signal cone_shape_changed

const LINE_WIDTH = 1.05
const LINE_COLOR = Color.blue

export(int, FLAGS, "Night vision", "X-Ray vision") var vision_near : int = VISION_NIGHT

export(float, 0.1, 30, 0.1) var total_radius = 4.0 setget _set_total_radius # in tiles
export(float, 0, 180, 0.5) var angle = 60 setget _set_angle
export(float, 0, 1, 0.01) var base_height = 0.0 setget _set_base_height
export(float) var near_radius = 1.0 setget _set_near_radius # in tiles

var radius : float
var collision_shape : ConeShape2D


func _ready() -> void:
	radius = total_radius * Config.tile_size
	_rebuild_polygon()


func can_see(_entity: Entity) -> bool:
	if is_instance_valid(_entity):
		# Ignore inactive entities. 
		# Change this if finding dead entities is desirable for gameplay
		if not _entity.is_active:
			return false

		if not overlaps_body(_entity):
			return false

		var _distance = to_local(_entity.position).length()
		var flags = _get_vision_flags(_distance)
		
		if flags & VISION_NIGHT or _entity.get_attribute("illuminance", 1.0) > 0:
			if flags & VISION_XRAY:
				return true
			
			var hit = Util.raycast(global_position, _entity.global_position, [self], Util.LAYER2D_STATIC | _entity.collision_layer)
			return hit and hit.collider == _entity
	
	return false


func _get_vision_flags(distance: float) -> int:
	if distance < near_radius * Config.tile_size:
		return vision_near
	return vision


func _set_total_radius(value: float) -> void:
	total_radius = value
	radius = value * Config.tile_size
	_update_near_radius()
	_rebuild_polygon()


func _set_near_radius(value: float) -> void:
	near_radius = value 
	_update_near_radius()
	emit_signal("cone_shape_changed")


func _set_base_height(value):
	base_height = value
	_rebuild_polygon()


func _set_angle(value):
	angle = value
	_rebuild_polygon()


func _update_near_radius() -> void:
	near_radius = clamp(near_radius, 0, total_radius)


func _rebuild_polygon() -> void:
	var collision_polygon = get_node_or_null("CollisionShape2D")
	if collision_polygon and collision_polygon.is_inside_tree():
		if not collision_polygon.shape is ConeShape2D:
			collision_polygon.shape = ConeShape2D.new(radius, angle, base_height)
		collision_polygon.shape.initialize(radius, angle, base_height)
		collision_shape = collision_polygon.shape
	emit_signal("cone_shape_changed")
