tool
class_name Vision
extends Area2D

signal target_detected(target)
signal target_changed(target)
signal clue_detected(clue)

# vision bitflags
const VISION_NORMAL: int= 0
const VISION_NIGHT: int = 1
const VISION_XRAY: int = 2

export(float, 0, 100, 0.1) var target_lost_search_duration = 0.0
export(int, FLAGS, "Night vision", "X-Ray vision") var vision := VISION_NORMAL

var targets := []
var target: Entity setget _set_target


func _ready() -> void:
	connect("body_entered", self, "_on_body_entered")
	connect("body_exited", self, "_on_body_exited")


func _process(delta: float) -> void:
	if can_see(target):
		return

	if not targets:
		set_process(false)

	_update_target()


func _update_target() -> void:
	var _target = null
	for t in targets:
		t.can_see = can_see(t.entity)
		if t.can_see:
			if _target == null:
				_target = t.entity

	if _target:
		_set_target(_target)
		return
	
	if target:
		call_deferred("emit_signal", "clue_detected", Clue.new(\
			Clue.ClueCategory.TARGET_LOST, \
			target.global_position, target, target_lost_search_duration))
	
	self.target = null


func can_see(_entity: Entity) -> bool:
	if is_instance_valid(_entity):
		# Ignore inactive entities. 
		# Change this if finding dead entities is desirable for gameplay
		if not _entity.is_active:
			return false

		if not overlaps_body(_entity):
			return false

		if vision & VISION_NIGHT or _entity.get_attribute("illuminance", 1.0) > 0:
			if vision & VISION_XRAY:
				return true
			
			var hit = Util.raycast(global_position, _entity.global_position, [self], Util.LAYER2D_STATIC | _entity.collision_layer)
			return hit and hit.collider == _entity
	
	return false


func _set_target(value: Entity) -> void:
	if target != value:
		target = value
		emit_signal("target_changed", target)


func _on_body_entered(body: Node) -> void:
	var _target: Target = Target.new(body, false, 0.0)

	targets.push_back(_target)
	_target.connect("target_spotted", self, "_on_target_spotted")
	set_process(true)


func _on_body_exited(body: Node) -> void:
	for _target in targets:
		if _target.entity == body:
			targets.erase(_target)
			if target == _target.entity:
				self.target = null
			return
	
	Debug.log("Error? Lost track of a target that wasn't in the target list.")


func clue_detected(clue: Clue) -> void:
	if owner.is_clue_valid(clue):
		emit_signal("clue_detected", clue)


func _on_target_spotted(target_entity: Node) -> void:
	emit_signal("target_detected", target_entity)


class Target:

	signal target_spotted(target_entity)

	var can_see: bool = false setget _set_can_see
	var entity: Node
	var spotted_time: float = 0.0

	func _init(_entity: Node, _can_see: bool, _spotted_time: float) -> void:
		entity = _entity
		can_see = _can_see
		spotted_time = _spotted_time

	func _set_can_see(value: bool) -> void:
		if can_see != value:
			can_see = value
			spotted_time = OS.get_ticks_msec() * 0.001

			if can_see:
				emit_signal("target_spotted", entity)
