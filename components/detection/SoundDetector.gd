tool
class_name SoundDetector
extends Area2D

export(float) var tile_radius := 1.0 setget _set_tile_radius

signal clue_detected(clue)


func _ready() -> void:
	_set_tile_radius(tile_radius)


func clue_detected(clue: Clue) -> void:
	if owner.is_clue_valid(clue):
		emit_signal("clue_detected", clue)


func _set_tile_radius(value: float) -> void:
	tile_radius = value
	if get_node_or_null("CollisionShape2D"):
		($CollisionShape2D.shape as CircleShape2D).radius = tile_radius * Config.tile_size
		update()
