extends Area2D

signal changed(new_value)

var _lights = []
var illuminance = 0.0 setget _set_illuminance

onready var timer = $RefreshInterval


func _ready() -> void:
	_update_illuminance()
	
	timer.start()
	timer.connect("timeout", self, "_update_illuminance")


func calc_light_level() -> float:
	var is_lit := false
	for light in _lights:
		if not Util.raycast(global_position, light.global_position, [self], Util.LAYER2D_STATIC):
			# If there is line of sight this light can reach us
			if light.is_in_group(Group.LIGHT_SINK):
				return 0.0
			is_lit = true
	
	if is_lit:
		return 1.0
	return 0.0


func _update_illuminance() -> void:
	_set_illuminance(calc_light_level())


func _set_illuminance(value: float) -> void:
	if illuminance == value:
		return
	illuminance = value
	emit_signal("changed", illuminance)


func _on_area_entered(area: Area2D) -> void:
	_lights.append(area)


func _on_area_exited(area: Area2D) -> void:
	_lights.erase(area)
