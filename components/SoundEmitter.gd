class_name SoundEmitter
extends Area2D

var _sound_mods = []

onready var map = Group.get_single_node(Group.MAP)

export(Color) var ripple_color = Color("dd7db3ad")
export(Clue.ClueCategory) var clue_category
export(float, 0, 50, 0.1) var sound_radius = 1.0
export(float, 0, 100, 0.1) var search_duration = 0.0


func _ready() -> void:
	connect("area_entered", self, "_on_area_entered")
	connect("area_exited", self, "_on_area_exited")


func emit_sound(_sound_radius = null):
	var multiplier : float = 1.0
	var adder : float = 0.0
	
	if _sound_mods:
		multiplier = 0.0
		for sound_mod in _sound_mods:
			multiplier += (sound_mod as SoundModifier).loudness_multiplier
			adder += (sound_mod as SoundModifier).loudness_adder
	
	if _sound_radius:
		sound_radius = _sound_radius
	sound_radius = max(sound_radius * multiplier + adder, 0)
	
	# Spawn clue source
	var clue_source = Util.SoundClueSource(clue_category, global_position, sound_radius, search_duration, owner, ripple_color)
	map.add_child(clue_source)


func _on_area_entered(area: Area2D) -> void:
	_sound_mods.append(area)


func _on_area_exited(area: Area2D) -> void:
	_sound_mods.erase(area)
