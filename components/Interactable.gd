class_name Interactable
extends Area2D

signal used(user)

enum Type { UNKNOWN, DOOR, CHEST, ITEM }

export(String) var item_name : String
export(String) var use_string_prefix : String
export(Type) var type = Type.UNKNOWN


func interact(user: Node2D) -> void:
	emit_signal("used", user)
