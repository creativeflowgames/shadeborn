extends Node2D

func play(sound_type: String) -> void:
	if has_node(sound_type):
		get_node(sound_type).play()
