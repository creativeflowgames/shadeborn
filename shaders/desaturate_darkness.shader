shader_type canvas_item;

uniform float darkness_saturation : hint_range(0.0, 1.0, 0.01) = 0.5;
uniform float darkness_value : hint_range(0.0, 1.0, 0.01) = 1.0;


vec3 hsv2rgb( in vec3 c )
{
    vec3 rgb = clamp( abs(mod(c.x*6.0+vec3(0.0,4.0,2.0),6.0)-3.0)-1.0, 0.0, 1.0 );

	return c.z * mix( vec3(1.0), rgb, c.y);
}


vec3 rgb2hsv(in vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}


void fragment() {
	float light_mask = float(AT_LIGHT_PASS);
	float dark_mask = 1.0 - light_mask;

	vec4 _texture = textureLod(TEXTURE, UV, 0.0).rgba;
	
	vec3 lit_color = _texture.rgb * light_mask;
	vec3 unlit_color = _texture.rgb * dark_mask;
	
	
	unlit_color = rgb2hsv(unlit_color);
	unlit_color.y *= darkness_saturation;
	unlit_color.z *= darkness_value;
	
	COLOR.rgb = lit_color + hsv2rgb(unlit_color);
	COLOR.a = _texture.a;
}

void light() {
	
}