/*
	Adapted from: https://www.shadertoy.com/view/Xt2GRD
*/

shader_type canvas_item;
render_mode unshaded;

uniform float Radius : hint_range(0, 1) = 0.0;
uniform float Width : hint_range(0.01, 100) = 0.1;
uniform float Alpha : hint_range(0, 1) = 1.0;

float sinc(float r, float width)
{
    width *= Width / 10.0;
    
    float scale = 1.0;
    
    float N = 1.1;
    float numer = sin(r / width);
    float denom = (r /width);
    
    if(abs(denom) <= 0.1 ) return scale;
    else return scale * abs(numer / denom);
}

float expo(float r, float dev)
{
    return 1.0 * exp(- r*r / dev);
}

void fragment(){
	vec2 uv = UV;
	
	vec2 cdiff = abs(uv - 0.5) * 2.1;
	
	float myradius = length(cdiff) * mix(1.0, texture(TEXTURE, uv).r, 0.02);
	
	vec3 wave = texture(TEXTURE, vec2(myradius, 0.25)).rgb;
	
	float radius = 1.5 * TIME / 3.0;
	radius = Radius;
	
	float r = sin((myradius - radius));
	
	r = r * r;
	
	vec3 dev = wave * vec3(1.0/500.0);
	
	COLOR.rgb = vec3(sinc(r, dev.x), sinc(r, dev.y), sinc(r, dev.z));
	COLOR.a = sinc(r, dev.x) * clamp(Alpha, 0.0, 1.0);
	
//	float siny = sin(FRAGCOORD.y / 1.5);
//
//	COLOR = COLOR * vec4(1.0 - 0.5 * siny * siny);
	
//	COLOR = mix(COLOR, vec4(texture(TEXTURE, uv / 1000.0).xy * 0.3,
//			0.7 * sin(TIME) * sin(TIME), 0.0), 0.5);
}