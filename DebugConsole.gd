extends Control

const COMMAND_PREFIX = "command_"
const COMMAND_COLOR = Color.cornflower

var command_history = PoolStringArray()
var command_index = 0
var command_index_changed = false

onready var console : TextEdit = $CenterContainer/PanelContainer/MarginContainer/VBoxContainer/Console
onready var prompt : LineEdit = $CenterContainer/PanelContainer/MarginContainer/VBoxContainer/Prompt
onready var command_list : PoolStringArray = _get_command_list()


func _ready() -> void:
	set_visibility(false)
	for command in _get_command_list():
		console.add_keyword_color(command, COMMAND_COLOR)
	

func _input(event: InputEvent) -> void:
	if event is InputEventKey and event.is_pressed():
		if event.is_action("debug_console"):
			set_visibility(false)
			get_tree().set_input_as_handled()
		match event.scancode:
			KEY_UP, KEY_DOWN:
				command_index = clamp(command_index + (-1 if event.scancode == KEY_UP else 1), 0, len(command_history) - 1)
				command_index_changed = true
				prompt.text = command_history[command_index]
				prompt.grab_focus()
				prompt.caret_position = len(prompt.text)
				get_tree().set_input_as_handled()


func set_visibility(_visibility: bool = true):
	set_process_input(_visibility)
	visible = _visibility
	if visible:
		prompt.grab_focus()


func add_message(message: String) -> void:
	console.text = (console.text + "\n" + message).strip_edges()


func command_clear() -> void:
	console.text = ""


func command_shade() -> void:
	var player = Group.get_single_node(Group.PLAYER)
	if player:
		player.shade = player.MAX_SHADE


func command_reload() -> void:
	get_tree().reload_current_scene()


func command_help() -> void:
	add_message("Available commands: %s." % command_list.join(",  "))
	# TODO: use get_script().get_script_method_list() to get arguments


func _get_command_list() -> PoolStringArray:
	var _command_list = PoolStringArray()
	for method in get_method_list():
		if method["name"].begins_with(COMMAND_PREFIX):
			_command_list.append(method["name"].substr(len(COMMAND_PREFIX)))
	return _command_list


func _on_Console_text_changed() -> void:
	if not console.has_focus():
		console.scroll_vertical = console.get_line_count() - 1


func _on_Prompt_text_entered(new_text: String) -> void:
	var method_name : String = COMMAND_PREFIX + new_text
	if command_index != len(command_history):
		command_history.remove(command_index)
	command_history.append(new_text)
	command_index = len(command_history)
	if has_method(method_name):
		add_message("> %s" % new_text)
		call(method_name)
	else:
		add_message("Unknown command: '%s'. Type help to see a list of valid commands." % new_text.split(" ")[0])
	prompt.clear()
	prompt.placeholder_text = ""


func _on_Prompt_text_changed(new_text: String) -> void:
	if command_index_changed:
		command_index_changed = false
	else:
		command_index = len(command_history)

