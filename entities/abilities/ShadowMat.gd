tool
extends Node2D

export(float) var tile_radius setget _set_tile_radius

onready var sound_mod = $SoundMod
onready var timer = $Duration


func _ready() -> void:
	_set_tile_radius(tile_radius)


func _set_tile_radius(value: float) -> void:
	tile_radius = value
	if sound_mod: sound_mod.tile_radius = value


func _on_Duration_timeout() -> void:
	if Engine.editor_hint:
		return
	queue_free()
