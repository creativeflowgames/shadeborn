extends "res://entities/light/base/LightSink.gd"

export(float) var final_length_multiplier : float = 2.66667
export(float) var animation_duration: float = 1.0

var starting_length : float
var final_length : float

onready var shape : RectangleShape2D = trigger_shape.shape
onready var tween : Tween = $Tween
onready var timer : Timer = $Duration


func _ready() -> void:
	starting_length = shape.extents.x * 2.0
	final_length = starting_length * final_length_multiplier
	animate()
	$ActivationSound.play()


# Expects values between 0.0 and 1.0
func _set_length(percent: float) -> void:
	var length = (final_length - starting_length) * clamp(percent, 0.0, 1.0) + starting_length
	
	# Resize trigger shape while keeping it centered
	shape.extents.x = length * 0.5
	trigger_shape.position.x = (length - starting_length) * 0.5
	
	# Resize and keep origin for the light as well
	light.scale.x = range_lerp(percent, 0.0, 1.0, 1.0, final_length / starting_length)
	light.position.x = (length - starting_length) * 0.5
	
	var debug = trigger_shape.get_node_or_null("DebugLight")
	if debug:
		debug.update()


func animate() -> void:
		tween.interpolate_method(self, "_set_length", 0.0, 1.0, 
			animation_duration, Tween.TRANS_EXPO, Tween.EASE_OUT)
		tween.start()


func _on_Duration_timeout() -> void:
	queue_free()
