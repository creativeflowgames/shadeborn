extends KinematicBody2D

const ANIMATION_DURATION = 0.5

export(float) var initial_speed = 2.5
export(float) var throw_max_distance = 5
export(float) var throw_bounce_distance = 2

var remaining: float = throw_max_distance * Config.tile_size
var velocity: Vector2

onready var collision_shape: CircleShape2D = $CollisionShape2D.shape
onready var tween: Tween = $Tween
onready var timer: Timer = $Duration
onready var light: Area2D = $LightSink
onready var sprite: Sprite = $ProjectileSprite

onready var throw_sound := $ThrowSounds
onready var impact_sound := $ImpactSounds
onready var bounce_sound := $BounceSounds


func _ready() -> void:
	light.visible = false
	sprite.visible = true
	velocity = Vector2.RIGHT * initial_speed * Config.tile_size


func _physics_process(delta: float) -> void:
	var delta_vel = velocity * delta
	var collision: KinematicCollision2D = move_and_collide(delta_vel.rotated(rotation))

	if collision:
		remaining -= collision.travel.length()
		remaining = min(remaining, throw_bounce_distance * Config.tile_size)
		velocity = velocity.bounce(collision.normal)
		bounce_sound.play()
		# ? emit signal
	else:
		remaining -= delta_vel.length()

	if remaining <= 0:
		set_physics_process(false)
		explode()


func explode() -> void:
	timer.start()
	$ActivationSound.play()
	animate()


func animate() -> void:
	light.visible = true
	sprite.visible = false
	tween.interpolate_property(light, "tile_radius", 0, light.tile_radius, 
		ANIMATION_DURATION, Tween.TRANS_EXPO, Tween.EASE_OUT)
	light.tile_radius = 0.0
	tween.start()


func _on_Duration_timeout() -> void:
	queue_free()
