extends KinematicBody2D

const ANIMATION_DURATION = 0.5

export(float) var initial_speed = 2.5
export(float) var throw_max_distance = 5

var remaining: float = throw_max_distance * Config.tile_size
var velocity: Vector2

onready var sound_emitter := $SoundEmitter
onready var collision_shape: CircleShape2D = $CollisionShape2D.shape
onready var timer: Timer = $Duration
onready var sprite: Sprite = $ProjectileSprite

onready var throw_sound := $ThrowSounds
onready var impact_sound := $ImpactSounds



func _ready() -> void:
	sprite.visible = true
	velocity = Vector2.RIGHT * initial_speed * Config.tile_size


func _physics_process(delta: float) -> void:
	var delta_vel = velocity * delta
	var collision: KinematicCollision2D = move_and_collide(delta_vel.rotated(rotation))

	remaining -= delta_vel.length()

	if collision or remaining <= 0:
		set_physics_process(false)
		explode()


func explode() -> void:
	impact_sound.play()
#	sprite.visible = false
	sound_emitter.emit_sound()
	timer.start()



func _on_Duration_timeout() -> void:
	queue_free()
