tool
class_name SoundModifier
extends Area2D

export(float) var tile_radius := 1.0 setget _set_tile_radius
export(float) var loudness_multiplier := 1.0 setget _set_loudness_multiplier
export(float) var loudness_adder := 0.0 setget _set_loudness_adder


func _enter_tree() -> void:
	_set_tile_radius(tile_radius)


func _draw() -> void:
	# draw_arc(Vector2.ZERO, tile_radius, 0, TAU, 36, Color.blueviolet, 1.0, true)
	draw_circle(Vector2.ZERO, tile_radius * Config.tile_size, Color( 0.54, 0.17, 0.89, 0.5 ))


func _get_configuration_warning() -> String:
	var warning = ""
	if loudness_multiplier == 1.0: warning += "Loudness multiplier of 1 has no effect. "
	if loudness_adder == 0.0: warning += "Loudness adder of 0 has no effect. "
	return warning


func _set_loudness_multiplier(value: float) -> void:
	loudness_multiplier = value
	update_configuration_warning()


func _set_loudness_adder(value: float) -> void:
	loudness_adder = value
	update_configuration_warning()

	
func _set_tile_radius(value: float) -> void:
	tile_radius = value
	
	var collision = get_node_or_null("CollisionShape2D")
	if collision:
		(collision.shape as CircleShape2D).radius = tile_radius * Config.tile_size
		update()
