class_name Artifact
extends KinematicBody2D


enum ActivationType {INSTANT, CONTINUOUS}
enum DistractionType {SOUND, VISUAL}

export(String) var item_name : String = "Unknown artifact"
export(String) var use_string_prefix : String = "Pick up"

export(float, 0.0, 500.0, 1.0) var cost : float = 0.0
export(ActivationType) var activation_type = ActivationType.INSTANT

export(PackedScene) var ability_scene
export(Resource) var status_effect

export(DistractionType) var distraction_type = DistractionType.SOUND
export(float) var distraction_radius = 10.0 # only used for sound distraction
export(float) var throw_max_distance = 10
export(float) var throw_bounce_distance = 1

export(Texture) var ability_icon: Texture

var remaining: float = throw_max_distance * Config.tile_size
var velocity: Vector2

onready var sound_emitter := $SoundEmitter
onready var collision_shape := $CollisionShape2D
onready var interactable := $Interactable
onready var throw_sound := $ThrowSounds
onready var impact_sound := $ImpactSounds
onready var bounce_sound := $BounceSounds


func _ready() -> void:
	set_physics_process(false)
	interactable.item_name = item_name
	interactable.type = Interactable.Type.ITEM
	interactable.use_string_prefix = use_string_prefix


func _physics_process(delta: float) -> void:
	var delta_vel = velocity * delta
	var collision: KinematicCollision2D = move_and_collide(delta_vel)

	if collision:
		remaining -= collision.travel.length()
		remaining = min(remaining, throw_bounce_distance * Config.tile_size)
		velocity = velocity.bounce(collision.normal)
		bounce_sound.play()
		# ? emit signal
	else:
		remaining -= delta_vel.length()

	if remaining <= 0:
		set_physics_process(false)
		impact_sound.play()
		_spawn_clue_source()


func get_cost(delta: float) -> float:
	match activation_type:
		ActivationType.INSTANT:
			return cost
		ActivationType.CONTINUOUS:
			return cost * delta
	return 0.0


func interact(user: Node2D) -> void:
	get_parent().remove_child(self)
	user.pick_up(self)


func throw(_velocity: Vector2) -> void:
	velocity = _velocity
	collision_shape.disabled = false
	interactable.monitoring = false
	interactable.monitorable = false
	throw_sound.play()
	set_physics_process(true)


func _spawn_clue_source() -> void:
	match distraction_type:
		DistractionType.SOUND:
			sound_emitter.emit_sound(distraction_radius)
		DistractionType.VISUAL:
			var clue_source = Util.VisualClueSource(Clue.ClueCategory.ARTIFACT_THROWN, global_position)
			get_parent().add_child(clue_source)


func _to_string() -> String:
	return "Artifact: %s" % interactable.item_name
