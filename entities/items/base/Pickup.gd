class_name Pickup
extends Area2D
# Base class for entities that can be picked up when touched

signal picked_up(agent)

onready var is_active = true

func pick_up(agent: Node2D) -> void:
	is_active = false
	emit_signal("picked_up", agent)
	queue_free()
