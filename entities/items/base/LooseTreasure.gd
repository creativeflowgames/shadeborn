class_name LooseTreasure
extends Pickup

export(float, 0, 1000000, 1.0) var value = 0

func _enter_tree() -> void:
	add_to_group(Group.LOOSE_TREASURE)
