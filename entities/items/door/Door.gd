extends KinematicBody2D

signal state_changed(value)

var is_open = false setget _set_is_open

onready var sprite_l := $SpriteL
onready var sprite_r := $SpriteR
onready var interactable := $Interactable
onready var collision := $CollisionShape2D
onready var audio := $AudioStreamPlayer2D
onready var sound_emitter := $SoundEmitter

func _ready() -> void:
	add_to_group(Group.DOOR)
	emit_signal("state_changed", is_open)


func _set_is_open(value: bool) -> void:
	if value == is_open:
		return
	
	is_open = value
	
	emit_signal("state_changed", is_open)
	
#	visible = not is_open
	
	collision.disabled = is_open
	interactable.monitorable = not is_open
	interactable.monitoring = not is_open


func _on_Interactable_used(user) -> void:
	self.is_open = true
	
	var t = $Tween
	t.interpolate_property(sprite_l, "scale:x", 1.0, 0.0, audio.stream.get_length(), Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	t.interpolate_property(sprite_r, "scale:x", 1.0, 0.0, audio.stream.get_length(), Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	
	t.start()
	audio.play()
	
	sound_emitter.emit_sound()
	
	yield(t, "tween_completed")
	
	visible = not is_open
