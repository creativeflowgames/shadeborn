extends KinematicBody2D

export(PackedScene) var spawn_item

onready var audio := $AudioStreamPlayer2D
onready var sound_emitter := $SoundEmitter


func _on_Interactable_used(user: Node2D) -> void:
	if spawn_item:
		var item = spawn_item.instance()
		get_parent().add_child(item)
		item.global_position = global_position
	
	audio.play()
	
	visible = false
	$CollisionShape2D.disabled = true
	$Interactable/CollisionShape2D.disabled = true
	
	sound_emitter.emit_sound()
	
	
	yield(audio, "finished")
	queue_free()
