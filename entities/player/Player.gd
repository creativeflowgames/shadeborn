class_name Player
extends Entity

const RECOVER_SCORE_MULTIPLIER = 0.30
const ARTIFACT_SCORE = 60

signal shade_changed(new_value, max_value)
signal score_changed(new_value)
signal interactable_changed(new_value)
signal incapacitated(is_dead)
signal sacrifice_state_changed(new_state)
signal illuminance_changed(new_value)

enum SacrificeType { OPEN_DOOR, GAIN_SHADE }

export(Resource) var brink_status_effect
export(float, 0, 999) var MAX_SHADE = 100.0

export(float, 0, 50) var footstep_tile_radius = 1.0

# attributes
export(float, 0, 999, 0.5) var health = 100.0
var illuminance: float = 0.0

export(float, 0, 10) var artifact_throw_speed

export(PackedScene) var focus_artifact

var input_direction : Vector2 = Vector2()
var shade : float setget _set_shade
var score := Score.new()
var interactables = []
var active_artifact : Artifact = null
var focus_factor: float setget _set_focus_factor

onready var sprite := $Sprite
onready var light_detector := $LightDetector
onready var sound_emitter := $SoundEmitter
onready var inventory := $Inventory
onready var ability_cooldown : Timer = $AbilityCooldown
onready var invulnerability_timer : Timer = $InvulnerabilityTimer
onready var throw_timer : Timer = $ThrowTimer
onready var sacrifice_timer : Timer = $SacrificeTimer
onready var footstep_audio := $FootstepPlayer

onready var focus : Artifact = focus_artifact.instance()

onready var map: TileMapAStar  = Group.get_single_node(Group.MAP)


func _enter_tree() -> void:
	add_to_group(Group.PLAYER)
	connect("active_state_changed", self, "_on_active_state_changed")


func _ready() -> void:
	self.direction = Vector2.DOWN
	sprite.direction = direction
	
	self.shade = MAX_SHADE
	_set_illuminance(illuminance)
	
	light_detector.connect("changed", self, "_set_illuminance")
	
	Util.sfx_focus_factor(0.0)
	
	score.connect("score_changed", self, "_on_score_changed")


func _unhandled_input(event: InputEvent) -> void:
	for a in ["up", "down", "left", "right"]:
		if InputMap.action_has_event(a, event):
			_update_input_direction()
			break
	if event.is_action_pressed("use"):
		interact()
	elif event.is_action_pressed("swap_items"):
		stop_ability()
		inventory.swap()
	elif event.is_action_pressed("primary_ability"):
		use_ability(inventory.get_primary())
	elif event.is_action_released("primary_ability"):
		stop_ability()
	elif event.is_action_pressed("focus"):
		start_focus()
	elif event.is_action_released("focus"):
		stop_focus()
	elif event.is_action_pressed("sacrifice"):
		if can_sacrifice():
			throw_timer.start()
	elif event.is_action_released("sacrifice"):
		if not throw_timer.is_stopped():
			throw_artifact()
		cancel_sacrifice()


func _process(delta: float) -> void:
	if interactables.size() > 1:
		var closest = _closest_interactable()
		if closest != interactables.front():
			interactables.erase(closest)
			interactables.push_front(closest)
			emit_signal("interactable_changed", closest)
	
	if active_artifact and active_artifact.activation_type == Artifact.ActivationType.CONTINUOUS:
		var cost = active_artifact.cost * delta
		if shade >= cost:
			_set_shade(shade - cost)
		else:
			stop_ability()
	
	if is_focusing():
		var cost = focus.cost * delta
		if shade >= cost:
			_set_shade(shade - cost)
		else:
			stop_focus()


func _physics_process(delta: float) -> void:
	if not input_direction.is_equal_approx(Vector2.ZERO):
		self.direction = input_direction.normalized()
		sprite.direction = direction
		sprite.is_moving = true
	else:
		sprite.is_moving = false
	
	move(input_direction, delta)


func _update_input_direction() -> void:
	input_direction.x = Input.get_action_strength("right") - Input.get_action_strength("left")
	input_direction.y = Input.get_action_strength("down") - Input.get_action_strength("up")
	input_direction = input_direction.normalized()
   

func receive_damage(instigator: Entity) -> void:
	if invulnerability_timer.is_stopped():
		.receive_damage(instigator)


func pick_up(item: Artifact) -> void:
	if not inventory.was_picked_up_before(item):
		score.add("Item", item.item_name, ARTIFACT_SCORE)
	var removed = inventory.add(item)
	if removed:
		removed.global_position = global_position
		get_parent().add_child(removed)


func interact() -> void:
	if not interactables:
		return
	
	var object : Interactable = interactables.front()
	
	match object.type:
		Interactable.Type.DOOR:
			if can_sacrifice():
				sacrifice_artifact(SacrificeType.OPEN_DOOR)
				object.interact(self)
			else:
				Debug.log("Unlocking a door requires sacrificing an artifact.")
		Interactable.Type.CHEST, Interactable.Type.ITEM:
			object.interact(self)


func use_ability(artifact: Artifact) -> void:
	if not artifact or not ability_cooldown.is_stopped() or active_artifact:
		return
	
	var cost = artifact.get_cost(get_process_delta_time())
	if shade >= cost:
		_set_shade(shade - cost)
		if artifact.ability_scene:
			var ability = artifact.ability_scene.instance()
			get_parent().add_child(ability)
			ability.global_position = global_position
			ability.rotation = direction.angle()
		
		if artifact.status_effect:
			add_status_effect(artifact.status_effect)
		
		if Artifact.ActivationType.CONTINUOUS:
			active_artifact = artifact
		else:
			ability_cooldown.start()
	else:
		# Not enough shade to use artifact
		Debug.log("Can't use artifact %s: needs %.0f shade" % [artifact.item_name, cost])


func stop_ability() -> void:
	if active_artifact:
		if active_artifact.status_effect:
			remove_status_effect(active_artifact.status_effect)
		active_artifact = null
		ability_cooldown.start()


func start_focus() -> void:
	if not ability_cooldown.is_stopped():
		return
	
	var cost = focus.get_cost(get_process_delta_time())
	if shade >= cost:
		_set_shade(shade - cost)
		if focus.ability_scene:
			var ability = focus.ability_scene.instance()
			get_parent().add_child(ability)
			ability.global_position = global_position
			ability.rotation = direction.angle()
		
		if focus.status_effect:
			if not is_focusing():
				add_status_effect(focus.status_effect)
		
				$FocusSoundTween.stop_all()
				$FocusSoundTween.interpolate_method(self, "_set_focus_factor", focus_factor, 1.0, 1.0, Tween.TRANS_QUAD, Tween.EASE_OUT)
				$FocusSoundTween.start()
	else:
		Debug.log("Can't use focus: needs %.0f shade" % [cost])


func stop_focus() -> void:
	if focus.status_effect:
		if is_focusing():
			remove_status_effect(focus.status_effect)
	
			$FocusSoundTween.stop_all()
			$FocusSoundTween.interpolate_method(self, "_set_focus_factor", focus_factor, 0.0, 0.5, Tween.TRANS_SINE, Tween.EASE_IN)
			$FocusSoundTween.start()


func is_focusing() -> bool:
	return has_status_effect(focus.status_effect)

func can_sacrifice() -> bool:
	return inventory.get_primary() and active_artifact == null and ability_cooldown.is_stopped()


func sacrifice_artifact(type: int) -> void:
	var artifact = inventory.remove()
	if type == SacrificeType.OPEN_DOOR:
		$SacrificeSounds.play()
		Debug.log("Sacrificed %s to open door." % artifact)
		return
	if type == SacrificeType.GAIN_SHADE:
		$SacrificeSounds.play()
		var old_shade = shade
		self.shade += artifact.cost #TODO: Decide how much shade is recovered
		Debug.log("Gained %.2f shade after sacrificing %s" % [shade - old_shade, artifact])
		return
	

func throw_artifact() -> void:
	throw_timer.stop()
	ability_cooldown.start()

	var artifact = inventory.remove()
	get_parent().add_child(artifact)
	artifact.global_position = global_position
	
	artifact.throw(direction * artifact_throw_speed * Config.tile_size)

	Debug.log("%s thrown" % artifact)
	emit_signal("sacrifice_state_changed", "throw")

func cancel_sacrifice() -> void:
	if sacrifice_timer.is_stopped():
		return
	sacrifice_timer.stop()
	emit_signal("sacrifice_state_changed", "cancel")


func can_recover() -> bool:
	return not has_status_effect(brink_status_effect) and not inventory.empty()


func recover() -> void:
	invulnerability_timer.start()
	add_status_effect(brink_status_effect)
	inventory.remove()
	score.multiply("Other", "Recover", RECOVER_SCORE_MULTIPLIER)
	self.is_active = true


func set_input(enabled: bool) -> void:
	propagate_call("set_process_unhandled_input", [enabled])
	propagate_call("set_physics_process", [enabled])


func _set_shade(value: float):
	value = clamp(value, 0, MAX_SHADE)
	if shade == value:
		return
	shade = value
	emit_signal("shade_changed", shade, MAX_SHADE)


func _set_illuminance(value: float) -> void:
	sprite.modulate = Color.white.darkened(0.7).linear_interpolate(Color.white, value)
	if illuminance == value:
		return
	
	illuminance = value
	emit_signal("illuminance_changed", get_attribute("illuminance"))


func _set_focus_factor(value: float) -> void:
	focus_factor = value
	Util.sfx_focus_factor(focus_factor)


func _closest_interactable() -> Object:
	var closest
	var closest_distance = 999999999
	for item in interactables:
		var distance = item.global_position.distance_to(global_position)
		if distance < closest_distance:
			closest = item
			closest_distance = distance
	return closest


func _on_item_area_entered(item: Area2D) -> void:
	if item is LooseTreasure and item.is_active:
		score.add("Pick up", "Treasure", item.value)
		item.pick_up(self)
	
	if item is Interactable:
		interactables.append(item)
		
		if len(interactables) == 1:
			emit_signal("interactable_changed", interactables.front())


func _on_item_area_exited(item: Area2D) -> void:
	if item is Interactable and item in interactables:
		interactables.erase(item)
		
		if len(interactables) <= 1:
			emit_signal("interactable_changed", interactables.front() if interactables else null)
	

func _on_SacrificeTimer_timeout() -> void:
	sacrifice_artifact(SacrificeType.GAIN_SHADE)
	ability_cooldown.start()
	emit_signal("sacrifice_state_changed", "hold_end")


func _on_ThrowTimer_timeout() -> void:
	sacrifice_timer.start()
	emit_signal("sacrifice_state_changed", "hold_start")


func _on_active_state_changed(is_active: bool) -> void:
	set_input(is_active)

	if is_active:
		return
	
	emit_signal("incapacitated", has_status_effect(brink_status_effect))


func _on_Player_footstep() -> void:
	var tile_info = map.tile_info_at(global_position)
	footstep_audio.play(tile_info.name)
	
	var sound_radius = footstep_tile_radius * tile_info.loudness
	sound_emitter.emit_sound(sound_radius)


func _on_score_changed(value: float) -> void:
	emit_signal("score_changed", value)



class Score:
	
	signal score_changed(value)
	
	var list = []
	
	
	func add(_category: String, _name: String, _value: float):
		list.append({"category": _category, "name": _name, "value": _value})
		emit_signal("score_changed", calculate())
		Debug.log("Score is %.0f (%+.0f)" % [calculate(), _value]) 
	
	
	func multiply(_category: String, _name: String, factor: float):
		var current_score = calculate()
		add(_category, _name, -current_score + current_score * factor)
	
	
	func calculate() -> float:
		var total = 0.0
		for item in list:
			total += item.value
		return total
