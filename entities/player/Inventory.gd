extends Node

signal changed(items)

var items = []
var picked_up_items = []


func add(item: Artifact) -> Artifact:
	var removed = null
	if item:
		if full():
			removed = items.pop_front()
		items.push_front(item)
		
		if not item in picked_up_items:
			picked_up_items.append(item)
		
		emit_signal("changed", items)
		Debug.log("Inventory: added %s%s" % [item, (" (removed %s)" % removed) if removed else ""])
	return removed


func remove(index: int = 0) -> Artifact:
	if len(items) <= index:
		return null

	var removed = items[index]
	items.remove(index)
	emit_signal("changed", items)
	Debug.log("Inventory: removed %s" % removed)
	return removed


func swap() -> void:
	if full():
		items.invert()
		emit_signal("changed", items)
		Debug.log("Inventory: swaped items. Primary: %s" % items.front().item_name)


func empty() -> bool:
	return items.empty()


func full() -> bool:
	return items.size() >= 2


func get_primary() -> Artifact:
	return items.front() if len(items) else null


func was_picked_up_before(item: Artifact) -> bool:
	return item in picked_up_items
