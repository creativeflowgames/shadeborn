extends Camera2D


func _ready() -> void:
	var map_limits = Group.get_single_node(Group.MAP).get_used_rect()
	limit_left = map_limits.position.x * Config.tile_size
	limit_right = map_limits.end.x * Config.tile_size
	limit_top = map_limits.position.y * Config.tile_size
	limit_bottom = map_limits.end.y * Config.tile_size
