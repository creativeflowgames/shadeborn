extends ClueSource

export(PackedScene) var RippleScene
export(float, 0.1, 10, 0.1) var ripple_speed = 1.0

var ripple

func init(_category: int, _position: Vector2, _radius: float, _search_duration: float = 0.0, _instigator: Node2D = null, _ripple_color: Color = Color.transparent) -> void:
	category = _category
	global_position = _position
	radius = _radius
	duration = 0.1
	search_duration = _search_duration
	instigator = _instigator

	ripple = RippleScene.instance()
	ripple.duration = radius * ripple_speed
	ripple.radius = radius * Config.tile_size
	
	if _ripple_color != Color.transparent:
		ripple.color = _ripple_color

func _enter_tree() -> void:
	._enter_tree()
	
	ripple.global_position = global_position
	Group.get_single_node(Group.MAP).add_child(ripple)
