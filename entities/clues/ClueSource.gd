class_name ClueSource
extends Area2D

const notify_delay = 0.1

export(Clue.ClueCategory) var category
export(float, 0.0, 100000) var duration = 0.0

var radius : float = 1.0
var instigator : Node2D = null
var targets = []
var search_duration: float = 0.0


func _enter_tree() -> void:
	var timer = $Duration
	timer.autostart = duration != 0.0
	if duration:
		timer.wait_time = duration
	timer.one_shot = true
	
	$Trigger.shape.radius = radius * Config.tile_size


func _ready() -> void:
	yield($Duration, "timeout")
	queue_free()


func notify() -> void:
	var clue := Clue.new(category, global_position, instigator, search_duration)
	# TODO: Determine group leader
	
	# Send clue to everyone
	for target in targets:
		# TODO: set clue.leader if leader
		target.clue_detected(clue)
	
	
	if duration > 0:
		queue_free()
	else:
		for target in targets:
			if not overlaps_area(target):
				call_deferred("_remove_target", target)
		
		yield(get_tree().create_timer(notify_delay), "timeout")
		notify()


func _on_area_entered(area: Area2D) -> void:
	if area in targets:
		return
	
	if not area.has_method("clue_detected"):
		printerr("Node %s should handle clues but has no \"clue_detected()\" method." % area.name)
		return
	
	targets.append(area)
	if len(targets) == 1:
		$Duration.paused = true
		yield(get_tree().create_timer(notify_delay), "timeout")
		notify()


func _remove_target(target: Area2D) -> void:
	targets.erase(target)
