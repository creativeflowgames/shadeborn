extends ClueSource


func init(_category: int, _position: Vector2, _radius: float, _duration: float = 0.0, _search_duration: float = 0.0, _instigator: Node2D = null) -> void:
	category = _category
	global_position = _position
	radius = _radius
	duration = _duration
	search_duration = _search_duration
	instigator = _instigator

