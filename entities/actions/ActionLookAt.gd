tool
class_name ActionLookAt
extends Action

export(Util.Direction) var direction : int

var entity : Entity
var dir_vector : Vector2


func _to_string() -> String:
	return "Action: Look at %s" % Util.direction_string(direction)


func init(_entity: Entity):
	entity = _entity
	dir_vector = Util.unsnap(direction) * Config.tile_size


func execute(delta: float) -> bool:
	var target_position = entity.global_position + dir_vector
	if entity.look_to(target_position, entity.ang_vel, delta):
		return true
	return false


func _get_action_name() -> String:
	return "Look at"
