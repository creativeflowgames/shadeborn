tool
class_name ActionWait
extends Action

export(float, 0, 300, 0.1) var duration : float

var elapsed


func _to_string() -> String:
	return "Action: Wait %.1fs" % duration


func init(_entity: Entity) -> void:
	elapsed = 0.0


func execute(delta: float) -> bool:
	elapsed += delta
	return elapsed >= duration


func _get_action_name() -> String:
	return "Wait"
