tool
class_name ActionGoToObject
extends Action

export(NodePath) var target_node : NodePath

var entity : Enemy
var target : Node
var path := []


func _to_string() -> String:
	return "Action: Go to object %s" % target_node.get_name(target_node.get_name_count() - 1)


func init(_entity: Enemy) -> void:
	entity = _entity
	target = entity.get_node(target_node)

	entity.update_path(target.global_position)


func execute(delta: float) -> bool:
	return entity.move_to_path(delta)


func _get_action_name() -> String:
	return "Go to object"
