tool
class_name Action
extends Resource

export(String) var action_name setget _set_action_name, _get_action_name


func _set_action_name(value: String) -> void:
	pass


func _get_action_name() -> String:
	return "No action"

