extends Sprite

export(float, 0.1, 10.0, 0.1) var duration = 1.5
export(float, 1.0, 2000.0, 1.0) var radius = 64.0 setget _set_radius
export(Color) var color = Color("dd7db3ad")

var max_scale = Vector2(radius,radius) / texture.get_size() * 2

onready var tween := $Tween


func _set_radius(value: float):
	radius = value
	max_scale = Vector2(radius,radius) / texture.get_size() * 2
	scale = max_scale


func _enter_tree() -> void:
	$Tween.connect("tween_all_completed", self, "_on_Tween_all_completed")
	

func _ready() -> void:
	modulate = color
	play()


func play():
	tween.interpolate_property(self, "scale", 
		Vector2.ZERO, max_scale, 
		duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.interpolate_property(self, "modulate:a",  modulate.a,  0.0,
			duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()


func _on_Tween_all_completed():
	queue_free()
