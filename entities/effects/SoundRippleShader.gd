extends Sprite

const RippleMaterial = preload("res://materials/sound_ripple_material.tres")

export(float, 0.1, 10.0, 0.1) var duration = 1.5
export(float, 1.0, 2000.0, 1.0) var radius = 64.0 setget _set_radius

onready var tween := $Tween


func _set_radius(value: float):
	radius = value
	scale = Vector2(radius, radius) / texture.get_size() * 2


func _enter_tree() -> void:
#	material = RippleMaterial.duplicate(false)
	material.set_shader_param("Radius", 1.0)
	material.set_shader_param("Alpha", 0.5)
	
	$Tween.connect("tween_all_completed", self, "_on_Tween_all_completed")
	

func _ready() -> void:
	play()


func play():
#	tween.interpolate_property(material, "shader_param/Radius", 
#			0.0, 1.0, duration,
#			Tween.TRANS_LINEAR, Tween.EASE_IN);
#	tween.interpolate_property(material, "shader_param/Alpha", 
#		0.3, 0.0, duration * 0.8, 
#		Tween.TRANS_QUINT, Tween.EASE_IN_OUT, duration * 0.2);
	tween.interpolate_property(material, "shader_param/Alpha",  0.5,  0.0,
			duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()


func _on_Tween_all_completed():
	queue_free()
