tool
class_name Entity
extends KinematicBody2D
# Base class for character entities.

signal active_state_changed(is_active)
signal footstep

export(float, 0, 99999.0) var speed : float
export(float, 5, 360.0) var ang_vel : float = 180

var is_active := true setget _set_active
var status_effects = []
var velocity : Vector2 = Vector2.ZERO
var direction : Vector2 = Vector2.RIGHT setget _set_direction
var step_distance_progress : float = 0.0
export(Util.Direction) var initial_direction = Util.Direction.S setget _set_initial_direction
export(float, 0.1, 5) var step_distance = 0.6 # in tiles
export(float, 0, 5) var DISTANCE_THRESHOLD = 0.5 # in tiles


func _ready() -> void:
	self.direction = Util.unsnap(initial_direction)


func receive_damage(instigator: Entity) -> void:
	if is_active:
		self.is_active = false
		Debug.log("%s damaged %s" % [instigator.name, name])


func inflict_damage(entity: Entity) -> void:
	entity.receive_damage(self)


func can_see(_entity: Entity) -> bool:
	if is_instance_valid(_entity):
		if not _entity.is_active:
			return false
		if _entity.get_attribute("illuminance", 1.0) > 0:
			var hit = Util.raycast(global_position, _entity.global_position, [self], Util.LAYER2D_STATIC | _entity.collision_layer)
			if hit and hit.collider == _entity:
				return true
	return false


# Moves this entity towards `target_position` at its normal speed.
# Returns true if entity is less than `distance_threshold` px away from `target_position`.
func move_to(target_position: Vector2, delta: float, distance_threshold: float = DISTANCE_THRESHOLD) -> bool:
	var distance = max(0.0, global_position.distance_to(target_position) - distance_threshold)
	if is_zero_approx(distance):
		return true
	move(global_position.direction_to(target_position), delta)
	return false


# Makes the entity rotate to face `target` global position at `ang_vel` degrees per second.
# Returns true if looking directly at position.
func look_to(target: Vector2, _ang_vel: float, delta: float) -> bool:
	var cur_angle = direction.angle()
	var target_angle = to_local(target).angle()
	var new_angle = Util.lerp_angle_clamped(cur_angle, target_angle, delta, deg2rad(_ang_vel))
	self.direction = polar2cartesian(1.0, new_angle)
	if is_equal_approx(new_angle, target_angle):
		return true
	return false


# Move entity towards `_direction`.
func move(_direction: Vector2, delta: float) -> void:
	velocity = _direction * get_attribute("speed")
	move_and_slide(velocity, Vector2.ZERO, false, 4)

	if get_slide_count() > 0:
		for i in get_slide_count():
			var collision : KinematicCollision2D = get_slide_collision(i)
			step_distance_progress += collision.travel.length()
	else:
		step_distance_progress += (velocity * delta).length()

	if step_distance_progress >= step_distance * Config.tile_size:
		step_distance_progress = 0.0
		emit_signal("footstep")


# Reset entity's velocity.
func stop() -> void:
	velocity = Vector2.ZERO


func has_status_effect(effect: StatusEffect) -> bool:
	return effect in status_effects


func add_status_effect(effect: StatusEffect) -> void:
	if effect in status_effects:
		return
	
	status_effects.append(effect)
	Debug.log("%s: Added status effect %s" % [name, effect.name])
	
	if effect.duration > 0:
		yield(get_tree().create_timer(effect.duration), "timeout")
		remove_status_effect(effect)


func remove_status_effect(effect: StatusEffect) -> void:
	if not effect in status_effects:
		return
	
	status_effects.erase(effect)
	Debug.log("%s: Removed status effect %s" % [name, effect.name])


func get_attribute(attribute: String, default_value: float = 0.0) -> float:
	var value = get(attribute)
	if typeof(value) == TYPE_REAL:
		for effect in status_effects:
			value = effect.apply_modifier(attribute, value)
		return value
	return default_value


func set_collisions(enabled: bool) -> void:
	for shape_owner in get_shape_owners():
		shape_owner_set_disabled(shape_owner, not enabled)


func _set_initial_direction(value: int) -> void:
	initial_direction = value
	self.direction = Util.unsnap(initial_direction)


func _set_direction(value: Vector2) -> void:
	if not value.is_equal_approx(Vector2.ZERO):
		direction = value
		update()


func _set_active(value: bool) -> void:
	if is_active == value:
		return

	is_active = value

	stop()
	set_collisions(is_active)

	# TODO: properly handle animations in each class
	global_rotation_degrees = 0 if is_active else -90

	emit_signal("active_state_changed", is_active)
