tool
class_name Patrol
extends Resource

enum Behavior { LOOP, PING_PONG, RESTART }

export(Array, Resource) var actions
var action_index := -1
export(Behavior) var behavior = Behavior.LOOP


func _to_string() -> String:
	return "Patrol: (%s) %s" % [_behavior_string(behavior), String(actions)]


func current_action() -> Action:
	if not actions:
		return null
	return actions[action_index % len(actions)]


func next_action() -> Action:
	if is_last_action() and behavior == Behavior.PING_PONG:
		actions.invert()
	action_index += 1
	return current_action()


func nonempty() -> bool:
	return not actions.empty()


func is_last_action() -> bool:
	if not actions:
		return false
	
	match behavior:
		Behavior.LOOP:
			return false
		Behavior.PING_PONG, Behavior.RESTART:
			return action_index == len(actions) - 1
	return true

func _behavior_string(b: int) -> String:
	match b:
		Behavior.LOOP:
			return "Loop"
		Behavior.PING_PONG:
			return "PingPong"
		Behavior.RESTART:
			return "Restart"
	return "?"
