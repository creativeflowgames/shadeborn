extends StateMachine

var patrol : Patrol = Patrol.new()
var patrol_action : Action


func _ready() -> void:
	add_state("idle")
	add_state("patrol")

	call_deferred("set_state", states.idle)


func _enter_state(old_state, new_state) -> void:
	match new_state:
		states.patrol:
			patrol_action = patrol.next_action()
			patrol_action.init(entity)


func _state_logic(delta: float) -> void:
	match state:
		states.patrol:
			if patrol_action:
				if patrol_action.execute(delta):
					patrol_action = null


func _exit_state(old_state, new_state) -> void:
	pass


func _get_transition(delta: float) -> Object:
	match state:
		states.idle:
			if patrol.nonempty():
				return states.patrol
		states.patrol:
			if not patrol_action:
				if patrol.is_last_action():
					return states.idle
				return states.patrol
	return null
