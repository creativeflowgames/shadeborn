tool
extends Enemy

export(float, 0, 99999) var PLAYER_SPOT_SCORE_VALUE = 60
export(float, 0, 120, 0.1) var detection_interval = 20.0

onready var last_detection: float = get_ticks_sec()- detection_interval

onready var footstep_audio := $ArmorFootstepAudio


func _on_footstep() -> void:
	footstep_audio.play()


static func get_ticks_sec() -> float:
	return OS.get_ticks_msec() * 0.001


func _on_VisionCone_target_detected(target) -> void:
	if target is Player:
		var cur_time = get_ticks_sec()
		if cur_time - last_detection >= detection_interval:
			last_detection = cur_time
			(target.score as Player.Score).add("Other", "Spotted", -PLAYER_SPOT_SCORE_VALUE)
