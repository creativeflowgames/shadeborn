extends StateMachine

export (float) var IDLE_WAIT_TIME = 1.0
export (float) var DEFAULT_SEARCH_DURATION = 60.0
export (float) var SEARCH_RADIUS = 5.0
export (float) var RECOVER_WAIT_TIME = 4.0
export (float) var ATTACK_DISTANCE = Config.tile_size

var patrol: Patrol = Patrol.new()
var patrol_action: Action = null

var is_attacking: bool = false
var is_searching: bool = false
var search_duration: float = 0.0

var target: Entity = null
var clue: Clue = null setget _set_clue

onready var map: TileMapAStar = Group.get_single_node(Group.MAP)


func _ready() -> void:
	add_state("idle")
	add_state("patrol")
	add_state("chase")
	add_state("investigate")
	add_state("search")
	add_state("attack")
	add_state("recover")
	add_state("distracted")

	call_deferred("set_state", states.idle)


func _enter_state(old_state, new_state) -> void:
	match new_state:
		states.patrol:
			patrol_action = patrol.next_action()
			patrol_action.init(entity)
		states.attack:
			entity.attack(target)
		states.investigate:
			entity.update_path(clue.position)
		states.search:
			entity.update_path(clue.position)
			search_duration = clue.search_duration if clue.search_duration > 0 else DEFAULT_SEARCH_DURATION


func _state_logic(delta: float) -> void:
	match state:
		states.idle:
			pass
		states.patrol:
			if patrol_action:
				if patrol_action.execute(delta):
					patrol_action = null
		states.chase:
			if not target:
				return
			if entity.move_to(target.global_position, delta, ATTACK_DISTANCE):
				is_attacking = true
		states.investigate:
			if entity.move_to_path(delta):
				if clue.category == Clue.ClueCategory.ARTIFACT_THROWN:
					if entity.look_to(clue.position, entity.ang_vel, delta):
						set_state(states.distracted)
						return
				is_searching = true
		states.search:
			if entity.move_to_path(delta):
				entity.update_path(entity.random_point_in_radius(clue.position, SEARCH_RADIUS))


func _exit_state(old_state, new_state) -> void:
	match old_state:
		states.attack:
			is_attacking = false
			clue = null
		states.patrol:
			patrol_action = null
		states.investigate:
			if not new_state in [states.search, states.investigate]:
				clue = null
		states.search:
			is_searching = false
			if not new_state in [states.search, states.investigate]:
				clue = null


func _get_transition(delta: float) -> Object:
	match state:
		states.idle:
			if target:
				return states.chase
			if clue:
				return states.investigate
			if state_time >= IDLE_WAIT_TIME and patrol.nonempty():
				return states.patrol
		states.patrol:
			if target:
				return states.chase
			if clue:
				return states.investigate
			if not patrol_action:
				if patrol.is_last_action():
					return states.idle
				return states.patrol
		states.chase:
			if not target and clue:
				return states.investigate
			if is_attacking:
				return states.attack
		states.investigate:
			if target:
				return states.chase
			if is_searching:
				return states.search
		states.search:
			if target:
				return states.chase
			if state_time >= search_duration:
				return states.idle
		states.attack:
			if state_time >= 2.0:
				# TODO: replace with animation finished
				return states.recover
		states.recover:
			if state_time >= RECOVER_WAIT_TIME:
				return states.idle
	return null


func priority(_clue: Clue) -> float:
	if not _clue.category in entity.clue_priorities.keys():
		return 0.0
	return entity.clue_priorities[_clue.category]


func _on_clue_detected(new_clue: Clue) -> void:
	if target:
		return
	if clue:
		if clue.category == new_clue.category or priority(new_clue) > priority(clue):
			self.clue = new_clue
	else:
		self.clue = new_clue


func _on_VisionCone_target_changed(_target: Entity) -> void:
	target = _target


func _set_clue(value: Clue) -> void:
	if value != clue:
		clue = value

		# Restart state if clue updated
		if clue and state in [states.investigate, states.search]:
			call_deferred("set_state", states.investigate)
