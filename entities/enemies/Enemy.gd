tool
class_name Enemy
extends Entity
# Base class for enemies in the game

export(Dictionary) var clue_priorities = Dictionary()
export(Resource) var patrol_actions: Resource


var sprite: Sprite
var vision_cone
var path := PoolVector2Array()
var player

onready var state_machine: StateMachine = $StateMachine
onready var focus_sound: AudioStreamPlayer2D = $FocusSound
onready var map: TileMapAStar = Group.get_single_node(Group.MAP)
onready var hud = Group.get_single_node(Group.HUD)


func _init() -> void:
	if not patrol_actions:
		patrol_actions = Patrol.new()


func _ready() -> void:
	._ready()
	
	# DEBUG
	print("%s: My patrol is: %s" % [name, (patrol_actions.to_string() if patrol_actions.nonempty() else "null")])

	if Engine.editor_hint:
		return
	
	vision_cone = get_node_or_null("VisionCone")
	if vision_cone:
		vision_cone.rotation = direction.angle()
	sprite = get_node("Sprite")

	state_machine.entity = self
	if "patrol" in state_machine:
		state_machine.patrol = patrol_actions

	focus_sound.max_distance = Config.focus_max_distance * Config.tile_size
	focus_sound.play(rand_range(0.0, focus_sound.stream.get_length()))

	yield(owner, "ready")
	player = Group.get_single_node(Group.PLAYER)


func _physics_process(delta: float):
	if Engine.editor_hint:
		return

	state_machine._physics_process(delta)

	var moving = not velocity.is_equal_approx(Vector2.ZERO)
	if moving:
		self.direction = velocity.normalized()
		sprite.is_moving = moving

	velocity = Vector2.ZERO


func _process(delta: float) -> void:
	if Engine.editor_hint:
		return

	if vision_cone:
		vision_cone.rotation = lerp_angle(vision_cone.rotation, direction.angle(), delta * 10)


func can_see(_entity: Entity) -> bool:
	if vision_cone:
		return vision_cone.can_see(_entity)
	return .can_see(_entity)


func update_path(target_position: Vector2) -> void:
	target_position = sanitize_patrol_point(target_position)
	path = map.astar_get_path(global_position, target_position)


func update_path_dark(target_position: Vector2) -> void:
	target_position = sanitize_patrol_point(target_position)
	path = map.astar_get_path(global_position, target_position, false)


func random_point_in_radius(point: Vector2, radius: float) -> Vector2:
	for _tries in range(10):
		var random_point = map.astar_get_random_point_in_radius(point, radius)
		if map.astar_get_path(global_position, random_point):
			return random_point
	return point


# Returns true if path is empty (entity reached destination)
func move_to_path(delta: float) -> bool:
	if path.empty():
		return true
	if move_to(path[0], delta, DISTANCE_THRESHOLD * Config.tile_size):
		path.remove(0)
	return path.empty()


# If the entity can't fit on the given `patrol_point` return the closest point from the map.
func sanitize_patrol_point(patrol_point: Vector2):
	if test_move(Transform2D(0, patrol_point), Vector2.RIGHT):
		var new_point: Vector2 = map.astar_get_closest_point(patrol_point)
		print("%s: Bad patrol point found. Replaced %s with %s" % [name, patrol_point, new_point] )
		patrol_point = new_point
	return patrol_point


func is_clue_valid(clue: Clue) -> bool:
	return clue.category in clue_priorities.keys()


func attack(target: Entity) -> void:
	inflict_damage(target)
	
	var attack_sounds = get_node_or_null("AttackSounds")
	if attack_sounds:
		attack_sounds.play()


func _set_direction(value: Vector2) -> void:
	._set_direction(value)
	
	if Engine.editor_hint and has_node("VisionCone"):
		get_node("VisionCone").rotation = direction.angle()
	
	get_node("Sprite").direction = value


func _on_Enemy_active_state_changed(is_active: bool) -> void:
	if is_active:
		focus_sound.play() 
	else:
		focus_sound.stop()


func _on_FocusSound_finished() -> void:
	focus_sound.play()

	if not player.is_focusing():
		return

	if global_position.distance_to(player.global_position) < Config.focus_max_distance * Config.tile_size:
		var xform = get_canvas_transform()
		var pos = xform * global_position
		var v_rect = get_viewport_rect()
		pos = Vector2(clamp(pos.x, v_rect.position.x, v_rect.size.x), clamp(pos.y, v_rect.position.y, v_rect.size.y))
		spawn_ripple(pos)


func spawn_ripple(pos: Vector2):
	print(name, pos)
	var ripple = load("res://entities/effects/SoundRipple.tscn").instance()
	ripple.duration = 0.2
	ripple.radius = 2 * Config.tile_size
	ripple.color = Color.whitesmoke
	ripple.global_position = pos
	hud.add_child(ripple)
