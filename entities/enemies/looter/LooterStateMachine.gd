extends StateMachine

const GROUP_TREASURE = "loose_treasure"
export(float) var TREASURE_SEARCH_COOLDOWN = 2.0
export(float) var ATTACK_DISTANCE = 24.0
export(float) var CHARGE_TIME = 1.60
export(float) var RECOVER_TIME = 3.30
export(float) var ALERT_TIME = 5.00
export(float) var ATTACK_SPEED = 3 # in tiles per second
export(float) var DEFAULT_SEARCH_DURATION = 60.0
export(float) var SEARCH_RADIUS = 5.0

var alert_end_time = ALERT_TIME

var sorted_treasures = {}
var attack_distance: float

var target: Node2D
var clue: Clue setget _set_clue

var is_searching: bool = false
var search_duration: float = 0.0

onready var map: TileMapAStar = Group.get_single_node(Group.MAP)


func _ready() -> void:
	add_state("inactive")
	add_state("idle")
	add_state("loot")
	add_state("flee")
	add_state("investigate")
	add_state("search")
	add_state("charge")
	add_state("attack")
	add_state("recover")
	add_state("distracted")
	add_state("dead")

	call_deferred("set_state", states.inactive)

	entity.connect("active_state_changed", self, "_on_active_state_changed")


func _enter_state(old_state, new_state) -> void:
	match new_state:
		states.idle:
			_find_treasure()
		states.loot:
			entity.update_path_dark(clue.position)
		states.charge:
			var direction = entity.global_position.direction_to(target.global_position)
			entity.direction = direction
			attack_distance = entity.global_position.distance_to(target.global_position) + 2 * Config.tile_size
		states.investigate:
			alert_end_time = ALERT_TIME
			entity.path = PoolVector2Array()
		states.search:
			entity.update_path(clue.position)
			search_duration = clue.search_duration if clue.search_duration > 0 else DEFAULT_SEARCH_DURATION


func _state_logic(delta: float) -> void:
	match state:
		states.investigate:
			if clue:
				if entity.path.empty():
					if entity.look_to(clue.position, entity.ang_vel, delta):
						entity.update_path_dark(clue.position)
				else:
					if entity.move_to_path(delta):
						if clue.category == Clue.ClueCategory.ARTIFACT_THROWN:
							if entity.look_to(clue.position, entity.ang_vel, delta):
								set_state(states.distracted)
								return
						is_searching = true
		states.search:
			if entity.move_to_path(delta):
				entity.update_path(entity.random_point_in_radius(clue.position, SEARCH_RADIUS))
		states.loot:
			if entity.move_to_path(delta):
				self.clue = null
		states.attack:
			var attack_speed: float = ATTACK_SPEED * Config.tile_size
			var collision : KinematicCollision2D = null
			if attack_distance >= ATTACK_DISTANCE:
				var vel : Vector2 = (entity.direction * attack_speed * delta).clamped(attack_distance)
				collision = entity.move_and_collide(vel)
				attack_distance -= vel.length()
				if not collision:
					return
				if collision.collider == target:
					entity.attack(target)
					set_state(states.idle)
				else:
					set_state(states.recover)
			else:
				set_state(states.recover)


func _exit_state(old_state, new_state) -> void:
	match old_state:
		states.loot, states.investigate:
			if new_state != states.investigate:
				self.clue = null
		states.attack:
			target = null
		states.search:
			is_searching = false
			if not new_state in [states.search, states.investigate]:
				clue = null


func _get_transition(delta: float) -> Object:
	match state:
		states.idle:
			if _can_attack():
				return states.charge
			if clue:
				if clue_is_loot():
					return states.loot
				else:
					return states.investigate
			if state_time >= TREASURE_SEARCH_COOLDOWN:
				return states.idle
		states.loot:
			if _can_attack():
				return states.charge
			if clue and not clue_is_loot():
				return states.investigate
			if not clue:
				return states.idle
		states.charge:
			if state_time >= CHARGE_TIME:
				return states.attack
		states.recover:
			if state_time >= RECOVER_TIME:
				return states.idle
		states.flee:
			if _can_attack():
				return states.charge
		states.investigate:
			if state_time >= alert_end_time:
				return states.idle
			if _can_attack():
				return states.charge
			if is_searching:
				return states.search
		states.search:
			if _can_attack():
				return states.charge
			if state_time >= search_duration:
				return states.idle
	return null


func clue_is_loot() -> bool:
	return clue.category == Clue.ClueCategory.TREASURE_DETECTED


func priority(_clue: Clue) -> float:
	if not _clue.category in entity.clue_priorities.keys():
		return 0.0
	return entity.clue_priorities[_clue.category]


func _sort_treasures_by_distance(treasures: Array) -> void:
	sorted_treasures = {}
	for treasure in treasures:
		var p = map.astar_get_path(entity.global_position, treasure.global_position)
		sorted_treasures[treasure] = len(p) if p else 99999999

	treasures.sort_custom(self, "_sort_by_distance")


func _sort_by_distance(a, b) -> bool:
	if sorted_treasures[a] < sorted_treasures[b]:
		return true
	return false


func _find_treasure() -> bool:
	var treasures = Group.get_active_nodes(Group.LOOSE_TREASURE)
	_sort_treasures_by_distance(treasures)

	if treasures:
		var treasure = treasures.front()
		self.clue = Clue.new(Clue.ClueCategory.TREASURE_DETECTED, treasure.global_position, treasure)
		if not treasure.is_connected("picked_up", self, "_on_treasure_picked_up"):
			treasure.connect("picked_up", self, "_on_treasure_picked_up", [treasure], CONNECT_ONESHOT)

		return true
	return false


func _can_attack() -> bool:
	return state_time >= 2.0 and target and entity.can_see(target)


func _on_VisibilityNotifier2D_screen_entered() -> void:
	if state == states.inactive:
		set_state(states.idle)


func _on_treasure_picked_up(agent: Node2D, treasure: LooseTreasure) -> void:
	if clue and clue.category == Clue.ClueCategory.TREASURE_DETECTED:
		self.clue = null


func _on_VisionCone_target_changed(_target: Entity) -> void:
	target = _target


func _on_clue_detected(new_clue: Clue) -> void:
	if target:
		return
	if clue:
		if clue.category == new_clue.category or priority(new_clue) > priority(clue):
			self.clue = new_clue
	else:
		self.clue = new_clue


func _on_active_state_changed(is_active: bool) -> void:
	if not is_active:
		set_state(states.dead)


func _set_clue(value: Clue) -> void:
	if value != clue:
		if value and clue == value and clue.category == value.category:
			entity.update_path_dark(clue.position)  # ? Might be buggy
		
		clue = value

		# ? Is this needed?
		# Restart state if clue updated
#		if clue and state in [states.investigate]:
#			call_deferred("set_state", states.investigate)
