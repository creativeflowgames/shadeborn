tool
extends Enemy

var score : float = 0


func _on_ItemDetector_area_entered(item: Pickup) -> void:
	if item is LooseTreasure:
		score += item.value
		item.queue_free()
