tool
extends "res://entities/light/base/LightSource.gd"

func _enter_tree() -> void:
	add_to_group(Group.STATIC_LIGHT)
