tool
extends Area2D

export(float, 0, 512, 0.1) var tile_radius = 1 setget _set_tile_radius
export(float, 0, 0.10, 0.01) var flicker_percent = 0.05

onready var light: Light2D = $Light2D
onready var trigger_shape: CollisionShape2D = $TriggerShape
onready var flicker_timer: Timer = $FlickerInterval


func _enter_tree() -> void:
	add_to_group(Group.LIGHT)


func _ready() -> void:
	randomize()


func _set_tile_radius(value: float) -> void:
	tile_radius = value
	var size = tile_radius * Config.tile_size
	
	if trigger_shape:
		if trigger_shape.shape is CircleShape2D:
			trigger_shape.shape.radius = size
		if trigger_shape.shape is RectangleShape2D:
			trigger_shape.shape.extents = Vector2(size, size)
	
	if light:
		light.texture_scale = (1.0 / (light.texture.get_size().x * 0.5)) * size


func _on_FlickerInterval_timeout() -> void:
	if not light: 
		return

	light.scale.x = move_toward(light.scale.x, 1.0 - randf() * flicker_percent, flicker_timer.wait_time * 0.1)
	light.scale.y = light.scale.x
