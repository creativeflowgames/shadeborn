tool
extends "res://entities/light/base/LightBase.gd"

func _enter_tree() -> void:
	add_to_group(Group.LIGHT_SOURCE)
