tool
extends Area2D

export(float, 0, 16) var light_energy = 2 setget _set_light_energy, _get_light_energy
export(float, 0.1, 10, 0.1) var light_scale = 1.25 setget _set_light_scale, _get_light_scale


var level: Level


func _enter_tree() -> void:
	add_to_group(Group.EXIT)


func _ready() -> void:
	if Engine.editor_hint:
		return
	$ExitPosition/Icon.visible = false


func _on_body_entered(body: Node) -> void:
	if Engine.editor_hint:
		return
	
	if body is Player:
		level.end_level()
	
	if body is Enemy:
		#TODO: move to ExitPosition then queue_free()
		body.queue_free()

func _set_light_energy(value: float) -> void:
	$Light.energy = value


func _get_light_energy() -> float:
	return $Light.energy


func _set_light_scale(value: float) -> void:
	$Light.texture_scale = value


func _get_light_scale() -> float:
	return $Light.texture_scale


