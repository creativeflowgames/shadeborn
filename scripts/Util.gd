tool
extends Node2D

const SoundClueSourceScene = preload("res://entities/clues/SoundClueSource.tscn")
const VisualClueSourceScene = preload("res://entities/clues/VisualClueSource.tscn")

enum Direction { E, NE, N, NW, W, SW, S, SE }

const RAYCAST_OFFSET = 3.0 # in pixels

const LAYER2D_STATIC 		= (1 << 0)
const LAYER2D_PICKUP 		= (1 << 1)
const LAYER2D_INTERACTABLE 	= (1 << 2)

const MAX_VOLUME_DB = 0
var MIN_VOLUME_DB = ProjectSettings.get_setting("audio/channel_disable_threshold_db")

var unfocus_bus = AudioServer.get_bus_index("SFX - Unfocus")
var focus_bus = AudioServer.get_bus_index("SFX - Focus")
var ambient_bus = AudioServer.get_bus_index("Ambient")





# https://stackoverflow.com/a/1437934
# Maps direction vector as follows:
# 	E -> 0   NE -> 1   N -> 2   NW -> 3
# 	W -> 4   SW -> 5   S -> 6   SE -> 7
static func snap(dir: Vector2) -> int:
	return ((int(round(atan2(-dir.y, dir.x) / (TAU / 8)))) + 8) % 8
	

# Maps integer to direction vector as follows:
# 	0 -> E   1 -> NE   2 -> N   3 -> NW
# 	4 -> W   5 -> SW   6 -> S   7 -> SE
static func unsnap(dir: int) -> Vector2:
	var angle = TAU/8*dir
	return Vector2(cos(angle), -sin(angle))


static func direction_string(dir: int) -> String:
	return Direction.keys()[dir]


# Like lerp_angle() but allows clamping angle delta before applying weight.
static func lerp_angle_clamped(from: float, to: float, weight: float = 1.0, max_distance: float = TAU):
	var difference = fmod(to - from, TAU)
	var distance = fmod(2.0 * difference, TAU) - difference
	return from + sign(distance) * min(abs(distance), max_distance * weight)


# Positions should be in world space
func raycast(from: Vector2, to: Vector2, exclude: Array = [], collision_layer: int = LAYER2D_STATIC, collide_with_bodies: bool = true, collide_with_areas: bool = false) -> Dictionary:
	# Move target towards source by `RAYCAST_OFFSET` to avoid overlaps with 
	# adjacent bodies. For instance, a torch close to a wall: if we raycast to
	# the torch's exact position it might hit the wall.
	#
	# Warning: this code is different from using to_local() on a Node as it
	# doesn't take the rotation into account.
	var to_local: Vector2 = to - from
	to = to_local.move_toward(Vector2.ZERO, RAYCAST_OFFSET)
	to = from + to
	
	var space_state = get_world_2d().direct_space_state
	return space_state.intersect_ray(from, to, 
		exclude, collision_layer, collide_with_bodies, collide_with_areas)


static func SoundClueSource(_category: int, _position: Vector2, _radius: float, _search_duration: float = 0.0, _instigator: Node = null, _ripple_color: Color = Color.transparent) -> Node2D:
	var clue_source = SoundClueSourceScene.instance()
	clue_source.init(_category, _position, _radius, _search_duration, _instigator, _ripple_color)
	return clue_source


static func VisualClueSource(_category: int, _position: Vector2, _radius: float = 1, _duration: float = 0, _search_duration: float = 0.0, _instigator: Node = null) -> Node2D:
	var clue_source = VisualClueSourceScene.instance()
	clue_source.init(_category, _position, _radius, _duration, _search_duration, _instigator)
	return clue_source


func sfx_focus_factor(focus: float) -> void:
	AudioServer.set_bus_volume_db(unfocus_bus, lerp(MIN_VOLUME_DB, MAX_VOLUME_DB, 1-focus))
	AudioServer.set_bus_volume_db(ambient_bus, lerp(MIN_VOLUME_DB, MAX_VOLUME_DB, 1-focus))
	
	AudioServer.set_bus_volume_db(focus_bus, lerp(MIN_VOLUME_DB, MAX_VOLUME_DB, focus))
