tool
extends Node
# Wrapper for properties in the Config category of Project Settings

const PREFIX = "config/"

const DEFAULT = {
	"tile_size": 128,
	"focus_max_distance": 15,
}


func _enter_tree() -> void:
	for property in DEFAULT:
		var full_property = PREFIX + property
		if not ProjectSettings.has_setting(full_property):
			ProjectSettings.set_setting(full_property, DEFAULT[property])		
		ProjectSettings.set_initial_value(full_property, DEFAULT[property])


func _set(property: String, value) -> bool:
	var full_property = PREFIX + property
	ProjectSettings.set_setting(full_property, value)
	ProjectSettings.save()
	return true


func _get(property: String):
	var full_property = PREFIX + property
	if ProjectSettings.has_setting(full_property):
		return ProjectSettings.get_setting(full_property)
	if property in DEFAULT:
		return DEFAULT[property]
	return null


func _get_property_list() -> Array:
	var list = []
	for item in ProjectSettings.get_property_list():
		if item["name"].begins_with(PREFIX):
			item["name"] = item["name"].substr(len(PREFIX))
			list.append(item)
	return list

