extends Node
class_name StateMachine

"""
Author: Game Endeavor
Source: https://www.youtube.com/watch?v=BNU8xNRk_oU

Base class for a simple state machine.
This is adequate for simple logic found in enemies.

---

Author: Alexandre Araújo

Added state_time variable and state_changed signal.
"""

signal state_changed(old, new)

var state = null setget set_state
var previous_state = null
var states = {}
var state_time = 0.0

onready var entity = owner


func _physics_process(delta: float):
	if state != null:
		_state_logic(delta)
		state_time += delta
		var new_state = _get_transition(delta)
		if new_state != null:
			set_state(new_state)
	

func _state_logic(delta: float):
	pass
	# match state:
	# 	states.idle:
	# 		pass


func _get_transition(delta: float):
	return null
	

func _enter_state(old_state, new_state):
	pass
	# match new_state:
	# 	states.idle:
	# 		pass
	

func _exit_state(old_state, new_state):
	pass
	# match old_state:
	# 	states.idle:
	# 		pass


func set_state(new_state):
	previous_state = state
	state = new_state
	state_time = 0.0
	
	if previous_state != null:
		_exit_state(previous_state, state)
	
	if new_state != null:
		_enter_state(previous_state, state)
	
	emit_signal("state_changed", state_name(previous_state), state_name(state))


func add_state(state_name: String):
	states[state_name] = states.size()


func state_name(state_index) -> String:
	if state_index == null:
		return ""
	return states.keys()[state_index]
