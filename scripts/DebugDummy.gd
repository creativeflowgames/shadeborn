extends CanvasLayer

enum Type { 
	NONE		= 0
	NORMAL		= 1 << 1, 
	LIGHTMAP	= 1 << 2,
	PATH		= 1 << 3,
}

var playing: bool
var debug : int = Type.NONE

func log(message: String):
	print(message)
