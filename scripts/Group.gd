tool
extends Node

const LOOSE_TREASURE = "loose_treasure"
const DEBUG = "debug"
const PLAYER = "player"
const MAP = "map"
const LIGHT = "light"
const LIGHT_SOURCE = "light_source"
const LIGHT_SINK = "light_sink"
const STATIC_LIGHT = "static_light"
const DYNAMIC_LIGHT = "dynamic_light"
const DOOR = "door"
const HUD = "hud"
const GAME = "game"
const EXIT = "exit"


func get_single_node(group_name: String) -> Node:
	return get_tree().get_nodes_in_group(group_name).pop_front()


func get_active_nodes(group_name: String) -> Array:
	var nodes = []
	for node in get_tree().get_nodes_in_group(group_name):
		if is_node_active(node):
			nodes.append(node)
	return nodes


func is_node_active(node: Node) -> bool:
	if node == null:
		return false
	if "is_active" in node:
		return node.is_active
	return true
