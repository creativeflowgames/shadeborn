extends CanvasLayer

const DebugConsoleScene = preload("res://DebugConsole.tscn")

enum Type { 
	NONE		= 0
	NORMAL		= 1 << 1, 
	LIGHTMAP	= 1 << 2,
	PATH		= 1 << 3,
}

signal logged(message)

var playing: bool = false setget set_playing
var debug_console : Control
var debug : int = Type.NONE


func _ready() -> void:
	set_playing(playing)


func _input(event: InputEvent) -> void:
	if event.is_action_released("debug_normal"):
		debug ^= Type.NORMAL
		_update_debug(debug)
	elif event.is_action_released("debug_path"):
		debug ^= Type.PATH
		_update_debug(debug)
	elif event.is_action_released("debug_lightmap"):
		debug ^= Type.LIGHTMAP
		_update_debug(debug)
	elif event.is_pressed() and event.is_action("debug_console"):
		if not debug_console or debug_console.visible:
			return
		debug_console.set_visibility(true)
		get_tree().set_input_as_handled()
	elif event is InputEventMouseButton:
		var player = Group.get_single_node(Group.PLAYER)
		match event.button_index:
			BUTTON_WHEEL_UP:
				player.get_node("Camera2D").zoom *= 0.9
			BUTTON_WHEEL_DOWN:
				player.get_node("Camera2D").zoom *= 1.1


func set_playing(value: bool) -> void:
	playing = value
	set_process_input(value)
	
	if playing:
		_update_debug(debug)
		debug_console = DebugConsoleScene.instance()
		add_child(debug_console)


func log(message: String):
	print(message)
	if is_instance_valid(debug_console):
		debug_console.add_message(message)
		emit_signal("logged", message)


func _update_debug(value: int) -> void:
	for debug_node in Group.get_active_nodes(Group.DEBUG):
		if debug_node.has_method("_on_debug_changed"):
			debug_node._on_debug_changed(debug)
		else:
			print_debug("Debug node without correct method: %s" % debug_node.name)
	
