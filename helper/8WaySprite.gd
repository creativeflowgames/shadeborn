tool
extends Sprite

signal footstep


export(Util.Direction) var preview_direction = Util.Direction.E setget _set_preview_direction
export(bool) var preview_spin = false setget _set_preview_spin

var is_moving: = false setget _set_moving
var direction: = Vector2.RIGHT setget _set_direction

onready var anim_player: AnimationPlayer = $AnimationPlayer


func _ready() -> void:
	if Engine.editor_hint:
		_set_preview_spin(preview_spin)
		return
	
	anim_player.play("idle")


func _enter_tree() -> void:
	if not is_connected("texture_changed", self, "_on_texture_changed"):
		connect("texture_changed", self, "_on_texture_changed")


# Select animation based on character movement
func _set_moving(value: bool) -> void:
	if is_moving == value: return
	is_moving = value
	
	anim_player.get_animation("move").loop = is_moving
	if is_moving:
		anim_player.play("move")
		if hframes > 1:
			anim_player.seek(1.0 / (hframes - 1)) # Start on second frame
	else:
		var anim_percent = anim_player.current_animation_position / anim_player.current_animation_length
		if anim_percent <= 0.3:
			anim_player.play_backwards("move")
		elif anim_percent >= 0.7:
			pass
		else:
			anim_player.play("idle")


func _set_preview_direction(value: int) -> void:
	preview_direction = value
	region_rect.position.y = value * region_rect.size.y


func _set_preview_spin(value: bool) -> void:
	preview_spin = value
	get_node("AnimationPlayer").play("spin" if value else "idle", -1, 1.0)


func _set_direction(value: Vector2):
	if value == Vector2.ZERO: return
	direction = value
	
	# Sets sprite frames from the specified row
	region_rect.position.y = Util.snap(value) * region_rect.size.y


func _on_footstep():
	emit_signal("footstep")


func _on_texture_changed():
	if texture:
		region_rect.size = texture.get_size() * Vector2(1.0, 1.0 / 8.0)
