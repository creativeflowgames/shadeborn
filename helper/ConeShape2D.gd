tool
class_name ConeShape2D
extends ConvexPolygonShape2D

const MIN_POINT_COUNT = 3

export(float, 1, 99999, 0.1) var radius = 128.0 setget _set_radius
export(float, 0, 180, 0.5) var angle = 60 setget _set_angle
export(float, 0, 1, 0.01) var base_height = 0.0 setget _set_base_height


func _init(_radius: float = 1.0, _angle: float = 60.0, _base_height: float = 0) -> void:
	initialize(_radius, _angle, _base_height)


func initialize(_radius: float = 1.0, _angle: float = 60.0, _base_height: float = 0) -> void:
	_set_angle(_angle)
	_set_radius(_radius)
	_set_base_height(_base_height)


func _set_radius(value: float) -> void:
	radius = value
	_rebuild_polygon()


func _set_angle(value: float) -> void:
	angle = value
	_rebuild_polygon()
	

func _set_base_height(value: float) -> void:
	base_height = value
	_rebuild_polygon()


func _rebuild_polygon():
	var points = PoolVector2Array()

	if base_height == 0:
		points.append(Vector2.ZERO)
	else:
		points.append(Vector2.DOWN * base_height * radius * 0.5)
		points.append(Vector2.UP * base_height * radius * 0.5)

	var start_angle = -angle * 0.5
	var end_angle = angle * 0.5
	var point_count = int(range_lerp(angle, 1.0, 360.0, MIN_POINT_COUNT, 45))
	
	for i in point_count:
		var angle_i = range_lerp(i, 0, point_count - 1, start_angle, end_angle)
		points.append(polar2cartesian(radius, deg2rad(angle_i)))
	
	set_point_cloud(points)
