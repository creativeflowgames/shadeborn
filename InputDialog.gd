extends GridContainer

const MOUSE_BUTTONS = {
	BUTTON_LEFT: "Left",
	BUTTON_RIGHT: "Right",
	BUTTON_MIDDLE: "Middle",
	BUTTON_XBUTTON1: "Back",
	BUTTON_XBUTTON2: "Forward",
	BUTTON_WHEEL_UP: "Wheel up",
	BUTTON_WHEEL_DOWN: "Wheel down",
	BUTTON_WHEEL_LEFT: "Wheel left",
	BUTTON_WHEEL_RIGHT: "Wheel right"
}

const ACTIONS = [
	"up",
	"down",
	"left",
	"right",
	"use",
	"swap_items",
	"primary_ability",
	"focus",
	"sacrifice",
]


export(PackedScene) var InputButtonScene
export(ButtonGroup) var button_group

var input_action: InputAction


func _enter_tree() -> void:
	for action in ACTIONS:
		spawn_controls_for(action)


func _exit_tree() -> void:
	for child in get_children():
		if not child.name.begins_with("Header"):
			remove_child(child)


func spawn_controls_for(action: String) -> void:
	var label = Label.new()
	label.text = action.capitalize()
	label.align = Label.ALIGN_RIGHT
	add_child(label)
	
	for _input_action in input_actions_for_(action):
		var button = InputButtonScene.instance()
		button.group = button_group
		_input_action.button = button
		button.connect("toggled", self, "_on_button_toggled", [_input_action])
		if _input_action.event:
			button.text = event_to_string(_input_action.event)
		add_child(button)


func _input(event: InputEvent) -> void:
	if not event.is_pressed():
		return
	if input_action:
		if event.is_action("ui_cancel"):
			input_action.button.pressed = false
			input_action = null
			return
		
		if event.get_class() in ["InputEventJoypadMotion", "InputEventJoypadButton"]:
			if not input_action.is_gamepad:
				return
		elif event.get_class() in ["InputEventKey", "InputEventMouseButton"]:
			if input_action.is_gamepad:
				return
		else:
			return
			
		if input_action.event:
			InputMap.action_erase_event(input_action.action, input_action.event)
		InputMap.action_add_event(input_action.action, event)
		
		get_tree().set_input_as_handled()
		
		var button = input_action.button
		input_action.event = event
		button.disconnect("toggled", self, "_on_button_toggled")
		button.connect("toggled", self, "_on_button_toggled", [input_action])
		button.pressed = false
		button.text = event_to_string(event)
		
		input_action = null


func input_actions_for_(action: String) -> Array:
	var keyboard = []
	var joy = []
	
	for event in InputMap.get_action_list(action):
		if event is InputEventKey or event is InputEventMouseButton:
			keyboard.append(InputAction.new(action, event, false))
		if event is InputEventJoypadButton or event is InputEventJoypadMotion:
			joy.append(InputAction.new(action, event, true))
	
	while(keyboard.size() < 2):
		keyboard.append(InputAction.new(action, null, false))
	while(joy.size() < 2):
		joy.append(InputAction.new(action, null, true))
	
	return keyboard + joy


func event_to_string(event: InputEvent) -> String:
	if event is InputEventKey:
		return event.as_text()
	if event is InputEventJoypadMotion:
		return Input.get_joy_axis_string(event.axis)
	if event is InputEventJoypadButton:
		return Input.get_joy_button_string(event.button_index)
	if event is InputEventMouseButton:
		return "Mouse %s" % MOUSE_BUTTONS[event.button_index]
	return event.as_text()


func _on_button_toggled(button_pressed: bool, _input_action: InputAction) -> void:
	var button = _input_action.button
	if button_pressed:
		if input_action and input_action.button == _input_action.button:
			return
		
		input_action = _input_action
	
	
	button.update2(button_pressed)
	


class InputAction:
	var button: Control
	var action: String
	var event: InputEvent
	var is_gamepad: bool
	
	func _init(a: String, e: InputEvent, ig: bool) -> void:
		action = a
		event = e 
		button = null
		is_gamepad = ig


func _on_ResetButton_pressed() -> void:
	InputMap.load_from_globals()
	
	var parent = get_parent()
	parent.remove_child(self)
	parent.add_child(self)
	parent.move_child(self, 0)

