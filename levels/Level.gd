class_name Level extends Node2D

export(PackedScene) var HUDScene
export(PackedScene) var ScoreReportDialog = load("res://ui/dialogs/ScoreReportDialog.tscn")

var game
var hud


func _enter_tree() -> void:
	hud = HUDScene.instance()
	add_child(hud)


func _ready() -> void:
	Util.sfx_focus_factor(0)
	var player: Player = Group.get_single_node(Group.PLAYER)
	if player:
		hud.player = player
		player.connect("incapacitated", hud, "show_recover_dialog")
	
	for exit in Group.get_active_nodes(Group.EXIT):
		exit.level = self


func end_level() -> void:
	var dialog = ScoreReportDialog.instance()
	var player =  Group.get_single_node(Group.PLAYER)
	dialog.show_dialog(name, player.score)
	hud.add_child(dialog)
	player.set_input(false)
