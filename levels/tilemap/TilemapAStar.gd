tool
class_name TileMapAStar
extends TileMap

enum ConnectDiagonals {NO, IGNORE_CORNERS, YES}

const DIRECTIONS = [
	Vector2.UP,
	Vector2.RIGHT,
	Vector2.DOWN,
	Vector2.LEFT
]

const DIAGONALS = [
	Vector2.UP + Vector2.RIGHT,
	Vector2.RIGHT + Vector2.DOWN,
	Vector2.DOWN + Vector2.LEFT,
	Vector2.LEFT + Vector2.UP
]
const DEFAULT_TILE_LOUDNESS = 1.0

export(float, 8, 1024, 1) var tile_size = Config.tile_size setget _set_tile_size, _get_tile_size
export(ConnectDiagonals) var connect_diagonals := ConnectDiagonals.IGNORE_CORNERS
export var tile_loudness = {}

export(float, 0.1, 250, 0.1) var LIGHT_WEIGHT_MULTIPLIER: float = 100.0
export(float, 0, 10, 0.1) var EXTRA_LIGHT_RADIUS: float = 1.0
export(float, 0.1, 5, 0.1) var LIGHT_RECALC_INTERVAL: float = 0.5
export(Curve) var LIGHT_WEIGHT_CURVE: Curve

var light_map : LightMap

var obstacle_tiles := []
var obstacles := []
var map_rect: Rect2
var astar: AStar2D
var astar_dark: AStar2D

var light_calc_interval : Timer

func _enter_tree() -> void:
	add_to_group(Group.MAP)
	
	# Add missing tiles to Tile Loudness and Obstacles
	for tile_id in tile_set.get_tiles_ids():
		var tile_name : String = tile_set.tile_get_name(tile_id)

		if tile_set.tile_get_navigation_polygon(tile_id):
			if Engine.editor_hint and not tile_loudness.has(tile_name):
				tile_loudness[tile_name] = DEFAULT_TILE_LOUDNESS

		if tile_set.tile_get_shape_count(tile_id) > 0:
			obstacle_tiles.append(tile_name)
	
	property_list_changed_notify()

	if Engine.editor_hint:
		return

	astar = AStar2D.new()
	astar_dark = AStar2D.new()
	map_rect = get_used_rect()
	light_map = LightMap.new(self, astar_dark)

	# Build map of tileId -> loudness
	var names = tile_loudness.keys()
	for name in names:
		if not name is String: continue
		var tile_id : int = tile_set.find_tile_by_name(name)
		if not tile_id == -1:
			tile_loudness[tile_id] = tile_loudness[name]
		tile_loudness.erase(name)

	# Generate obstacle list
	for name in obstacle_tiles:
		var obstacle_id : int = tile_set.find_tile_by_name(name)
		obstacles += get_used_cells_by_id(obstacle_id) 

	var passable_cells : PoolVector2Array = astar_add_passable_cells(obstacles)
	astar_connect_passable_cells(passable_cells)
	
	light_calc_interval = Timer.new()
	light_calc_interval.wait_time = LIGHT_RECALC_INTERVAL
	light_calc_interval.connect("timeout", self, "_recalculate_light_weights", [LightMap.Type.DYNAMIC])
	add_child(light_calc_interval)


func _ready() -> void:
	if Engine.editor_hint:
		return


	for door in Group.get_active_nodes(Group.DOOR):
		door.connect("state_changed", self, "_on_door_state_changed", [door])
		_on_door_state_changed(door.is_open, door)


	_recalculate_light_weights(LightMap.Type.STATIC | LightMap.Type.DYNAMIC)
	light_calc_interval.start()


func _recalculate_light_weights(flags: int) -> void:
#	var time = OS.get_ticks_msec()
	light_map.recalculate_light(flags)
	
	# DEBUG
#	print("Calculated %s lighting in %d ms." % [
#		_flags_string(flags), 
#		(OS.get_ticks_msec() - time)])


func astar_add_passable_cells(_obstacles = []) -> PoolVector2Array:
	var points_array = PoolVector2Array()
	for point in get_used_cells():
		if point in _obstacles:
			continue

		points_array.append(point)
		astar_add_point(point)

	return points_array


func astar_connect_passable_cells(points_array: PoolVector2Array) -> void:
	for point in points_array:
		var connections = []

		for direction in DIRECTIONS:
			var point_relative : Vector2 = point + direction
			if is_cell_passable(point_relative):
				connections.append(direction)

		if connect_diagonals != ConnectDiagonals.NO:
			for diagonal in DIAGONALS:
				var point_relative = point + diagonal
				if not is_cell_passable(point_relative):
					continue
				
				match connect_diagonals:
					ConnectDiagonals.YES:
						if Vector2(diagonal.x, 0) in connections or Vector2(0, diagonal.y) in connections:
							connections.append(diagonal)
					ConnectDiagonals.IGNORE_CORNERS:
						if Vector2(diagonal.x, 0) in connections and Vector2(0, diagonal.y) in connections:
							connections.append(diagonal)
		
		for connection in connections:
			astar_connect_points(point, point + connection)


# Ensure cost >= 1
func astar_add_point(point: Vector2, cost: float = 1.0) -> void:
	astar.add_point(get_point_id(point), point, cost)
	astar_dark.add_point(get_point_id(point), point, cost)


func astar_connect_points(a: Vector2, b: Vector2) -> void:
	astar.connect_points(get_point_id(a), get_point_id(b), false)
	astar_dark.connect_points(get_point_id(a), get_point_id(b), false)


func astar_get_path(a: Vector2, b: Vector2, ignore_darkness: bool = true) -> PoolVector2Array:
	var _astar = astar if ignore_darkness else astar_dark

	var point_a = world_to_map(a)
	var point_b = world_to_map(b)
	
	if not _astar.has_point(get_point_id(point_a)) or not _astar.has_point(get_point_id(point_b)):
		return PoolVector2Array()

	var path = _astar.get_point_path(get_point_id(point_a), get_point_id(point_b))

	if path.empty(): 
		return path

	for i in path.size():
		path[i] = _map_to_world(path[i])

	# replace first and last points with world positions
	path[0] = a
	path[-1] = b

	return path


func astar_get_closest_point(to_position: Vector2, ignore_darkness: bool = true):
	var _astar = astar if ignore_darkness else astar_dark
	return _map_to_world(_astar.get_closest_position_in_segment(world_to_map(to_position)))


func astar_get_random_point_in_radius(origin: Vector2, radius: float) -> Vector2:
	var point = origin + polar2cartesian(rand_range(1, radius) * Config.tile_size, randf() * 360.0)
	return astar_get_closest_point(point)


func is_cell_passable(p: Vector2) -> bool:
	return astar.has_point(get_point_id(p))

# Returns a dictionary with the keys {"name", "loudness"} for the tile at the
# specified world position.
func tile_info_at(global_position: Vector2) -> Dictionary:
	var tile_id = get_cellv(world_to_map(global_position))
	if not tile_id in tile_loudness:
		return {"name": "", "loudness": 1.0}
	var tile_name : String = tile_set.tile_get_name(tile_id)
	return {"name": strip_number(tile_name), "loudness": tile_loudness[tile_id] }


# Removes trailing numbers from tile types
# Example: "grass_16" -> "grass",  "stone_thing" -> "stone_thing"
func strip_number(tile_name: String) -> String:
	var result: PoolStringArray = tile_name.rsplit("_")
	if result[len(result) - 1].is_valid_integer():
		result.resize(len(result) - 1)
	return result.join("_")


func get_tile_loudness(global_position: Vector2) -> float:
	var tile_id = get_cellv(world_to_map(global_position))
	if not tile_id in tile_loudness:
		return 1.0
	return tile_loudness[tile_id]


# Return value should be >= 0 and unique
func get_point_id(point: Vector2) -> int:
	point -= map_rect.position
	return (point.x + map_rect.size.x * point.y) as int


# _draw() helper to draw lines between connections
func _draw_point_connections(id: int) -> void:
	var center_point = astar.get_point_position(id)
	for connection in astar.get_point_connections(id):
		var connection_pos = astar.get_point_position(connection)
		
		# Only draw half the connections since they're bidirectional
		if (connection_pos - center_point) in [Vector2.UP, Vector2.LEFT, Vector2.UP + Vector2.LEFT, Vector2.UP + Vector2.RIGHT]:
			continue
		
		draw_line(center_point * cell_size + cell_size * 0.5,
			 connection_pos * cell_size + cell_size * 0.5,
			 Color(1,1,0,0.5), 1.0, true)


# map_to_world with offset
func _map_to_world(position: Vector2) -> Vector2:
	return map_to_world(position) + cell_size * 0.5


func _get_tile_size() -> int:
	return Config.tile_size


func _set_tile_size(value: int) -> void:
	Config.tile_size = value
	tile_size = Config.tile_size
	cell_size = Vector2(tile_size, tile_size)


func _on_door_state_changed(is_open: bool, door: Node2D) -> void:
	var point : Vector2 = world_to_map(door.global_position)
	astar.set_point_disabled(get_point_id(point), not is_open)
	astar_dark.set_point_disabled(get_point_id(point), not is_open)
	Debug.log("Door state at %s changed. Point is now %s" % [point, "enabled" if is_open else "disabled"])


	
class LightMap:

	enum Type { NONE = 0, STATIC = 1, DYNAMIC = 2 }

	const MIN_LIGHT_WEIGHT: float = 1.0
	var LIGHT_WEIGHT_MULTIPLIER: float
	var EXTRA_LIGHT_RADIUS: float
	
 
	var astar: AStar2D
	var map: TileMapAStar
	var light_static := PoolRealArray()

	var static_lights = []
	var dynamic_lights = []

	var dynamic_lit_points = []

	func _init(_map: TileMapAStar, _astar: AStar2D) -> void:
		map = _map
		LIGHT_WEIGHT_MULTIPLIER = map.LIGHT_WEIGHT_MULTIPLIER
		EXTRA_LIGHT_RADIUS = map.EXTRA_LIGHT_RADIUS
		astar = _astar

		light_static.resize(int(map.map_rect.size.x * map.map_rect.size.y))


	func recalculate_light(flags: int = Type.DYNAMIC) -> void:
		if flags == Type.NONE:
			return

		if not static_lights:
			static_lights = map.get_tree().get_nodes_in_group(Group.STATIC_LIGHT)
		if not dynamic_lights:
			dynamic_lights = map.get_tree().get_nodes_in_group(Group.DYNAMIC_LIGHT)
		
		if not static_lights and not dynamic_lights:
			return
		
		if flags & Type.STATIC:
			for point_id in astar.get_points():
				var pos = map._map_to_world(astar.get_point_position(point_id))
				light_static[point_id] = _get_light_at(pos, true) * LIGHT_WEIGHT_MULTIPLIER
				astar.set_point_weight_scale(point_id, max(MIN_LIGHT_WEIGHT, light_static[point_id]))
		
		if flags & Type.DYNAMIC:
			# Reset dynamic light values for previous points
			for point_id in dynamic_lit_points:
				astar.set_point_weight_scale(point_id, max(MIN_LIGHT_WEIGHT, light_static[point_id]))
			dynamic_lit_points = []
			
			# Generate list of map points that may be dynamically lit
			for light in dynamic_lights:
				var r = ceil(light.tile_radius + EXTRA_LIGHT_RADIUS)
				for x in range(r * 2 + 1):
					for y in range(r * 2 + 1):
						var map_pos = map.world_to_map(light.global_position) + Vector2(x - r, y - r)
						var point_id = map.get_point_id(map_pos)
						if astar.has_point(point_id) and not point_id in dynamic_lit_points:
							dynamic_lit_points.append(point_id)
			
			# Update dynamic light info for possible lit points
			for point_id in dynamic_lit_points:
				var map_pos = astar.get_point_position(point_id)
				var pos = map._map_to_world(map_pos)
				var light = _get_light_at(pos, false) * LIGHT_WEIGHT_MULTIPLIER
				astar.set_point_weight_scale(point_id, max(MIN_LIGHT_WEIGHT, light_static[point_id] + light))


	func _get_light_at(world_pos: Vector2, is_static: bool) -> float:
		var light_nodes = static_lights if is_static else dynamic_lights
		var distance = 1000 * Config.tile_size
		var closest_light = null
		for light in light_nodes:
			var cur_dist = light.global_position.distance_to(world_pos)
			if cur_dist >= distance:
				continue
			distance = cur_dist
			closest_light = light
		var light_pos = closest_light.global_position
		var max_distance = (closest_light.tile_radius + EXTRA_LIGHT_RADIUS) * Config.tile_size
		if  distance <= max_distance:
			if not Util.raycast(world_pos, light_pos):
				var offset = clamp(distance / max_distance, 0.0, 1.0)
				return map.LIGHT_WEIGHT_CURVE.interpolate_baked(offset)
		return 0.0


static func _flags_string(flags: int) -> String:
	var f = PoolStringArray()
	if flags & LightMap.Type.STATIC: f.append("static")
	if flags & LightMap.Type.DYNAMIC: f.append("dynamic")
	return f.join(" and ")
